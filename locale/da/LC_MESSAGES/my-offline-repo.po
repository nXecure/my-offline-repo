# Danish translations for PACKAGE package.
# Copyright (C) 2020 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-08 12:14+0100\n"
"PO-Revision-Date: 2020-11-08 12:14+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:41
msgid "\\n Finished"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:42
msgid "This Machine"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:50
msgid "My OFFLINE repo"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:55
msgid "OS architecture: <b>$SYSTEM_ARCH</b>\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:56
msgid "Download a program + its dependencies"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:58
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:163
msgid "Download Updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:60
msgid "Install from this local repository"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:74
msgid "Download Programs"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:76
msgid "Select the program you want to download \\nand where to save it.\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:78
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:167
msgid "Folder"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:78
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:204
msgid "Package Name"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:79
msgid ""
"Select file!!Select a file containing a collection of packages you want to "
"download"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:81
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:170
msgid "Update package list!gtk-convert!Update package list (sudo apt update)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:83
msgid "!gtk-find!Search for selected package"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:84
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:173
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:213
msgid "!gtk-preferences!Configure Options"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:85
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:174
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:214
msgid "!gtk-go-back-ltr"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:122
msgid "Create packagelist"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:122
msgid ""
"Give a name to the new packagelist you want to create.\\nIt will contain the "
"list of packages installed\\n on the current machine.\\n\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:122
msgid "PackageList Name"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:122
msgid "Save packagelist!gtk-save!Save the list of packages on this machine"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:141
msgid ""
"There already is a file with the name\\n $NEW_PACKAGE_LIST \\n NO new "
"packagelist created."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:145
msgid ""
"A new packagelist named ${NEW_PACKAGE_LIST} was saved. \\nYou can verify it "
"by relaunching the current Updates window."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:152
msgid "Packagelist creation"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:165
msgid ""
"Select a System and a folder to save any packages.\\nCheck for updates and "
"download them.\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:167
msgid "Package List"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:168
msgid ""
"New Packagelist!!Create a new .packagelist file specifically for this machine"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:172
msgid ""
"Check!gtk-apply!Check for updates for the selected package list and download "
"them"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:198
msgid "Offline Repo Installer"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:200
msgid ""
"Select the folder where your repo is stored. Add repo.\\nSearch for a "
"program to install from repo.\\nRemove repo from sourcelist when finished.\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:204
msgid "Repo Folder"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:205
msgid "Upgrade!!Install all available updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:207
msgid ""
"Remove repo from list!gtk-remove!REMEMBER to ALWAYS remove the repo from the "
"sourcelist before exiting the program"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:209
msgid ""
"Add repo!gtk-add!Add this folder as an offline repo to the sources list and "
"update packagelist"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:211
msgid ""
"Search!gtk-find!Search for the package you want to install. Leave empty to "
"display all options."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:234
msgid ""
"<b>Download Programs</b> will let you download any package (along with all "
"its dependencies) and will update the local repo information. Follow these "
"steps:\\n\\n<b>1. Update package list</b> to ensure you are downloading the "
"newest programs.\\n\\n<b>2.</b> Select the <b>Folder</b> where you want "
"programs and their dependencies to be downloaded and saved. This folder will "
"become your Repo folder.\\n\\n<b>3A. </b>Write the <b>Package Name</b> you "
"want to add to your local repo. If you don't know the exact name, use as few "
"descriptive words as possible (example chrome for google-chrome).\\n"
"\\n<b>3B. If you already have a file with a list of packages you want to "
"download, click the </b>Select file</b> button to download packages listed "
"in that file. You can optionally give this selection a Name and Description "
"(easier to find later).\\n\\n<b>4. Search</b> (search button beside the "
"Package Name entry) will search for the name (or the words) you wrote in "
"<b>Package Name</b> and will give you a list of results.\\n\\n<b>5.</b> In "
"the popup search results window, select the package you want to download and "
"it will start building the dependency list. A confirmation window will "
"display information about the program/package selected and the amount of "
"dependencies it will download (You can check them clicking the <b>LIST</b> "
"button). If you agree, the download process will begin.\\n\\n\\nNote: "
"download options can be changed by clicking the 'Configuration "
"Options' (gear icon) button. Each option is described in the HELP window "
"launched from inside the 'CONFIGURATION' window."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:254
msgid ""
"<b>Download Updates</b> will let you check for any available updates for an "
"offline system and download them for you to a local folder.\\n\\nPackage "
"information for any offline system should be stored in a <b>.packagelist "
"file</b> inside the <i>packagelists</i> folder. You can either use the "
"already created ones there or create your own based on the installed "
"packages on your offline system. For that, you will need to launch this "
"program on the offline machine and click the <b>New Packagelist</b> button "
"to create the new packagelist.\\nStandard packagelists for antiX 19.3 "
"vanilla systems have already been created.\\n\\nTo check for updates, you "
"need to follow these steps:\\n<b>1. Update package list</b> to ensure you "
"are checking for the newest updates.\\n\\n<b>2.</b> Select the <b>Folder</b> "
"where you want your updates to be downloaded and saved. This folder will "
"become your Repo folder.\\n\\n<b>3.</b> Select the <b>Package List</b> that "
"is related to the system which you want to update. If there are no available "
"options, it means that no .packagelist files can be found inside the "
"packagelists folder.\\n\\n<b>4. Check</b> button will check for any updates "
"for the Package List selected. If any updates are found, a new window will "
"display a list of update. You can then choose to Download them or cancel the "
"download.\\n\\n\\nNote: download options can be changed by clicking the "
"'Configuration Options' (gear icon) button. Each option is described in the "
"HELP window launched from inside the 'CONFIGURATION' window."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:275
msgid ""
"<b>Offline Repo Installer</b> will let you add a folder containing a "
"collection of packages to your sources lists, acting as an offline local "
"repo.\\n\\n<b>1.</b> Select the <b>Repo Folder</b> that contains your "
"downloaded packages and dependencies.\\n\\n<b>2.</b> Click the <b>Add repo</"
"b> button (beside the <b>Repo Folder</b> entry) to add your repo directory "
"to the apt sources lists.\\n\\n<b>3A.</b> Write the <b>Package Name</b> you "
"want to install. Leave empty if you want to see all available programs from "
"the local Repo.\\n\\n<b>3B.</b> Click the <b>Upgrade</b> button if you only "
"want to install updates on your offline system. It should launch a new "
"terminal window displaying all updates available.\\n\\n<b>4. Search</b> for "
"all available packages that match the selected <b>Package Name</b>. If there "
"are .repo files in the repo folder, it will display them at the top of the "
"search results. Install the package selected on your current system.\\n"
"\\n<b>5. Remove repo from list</b> will delete all local repos from your "
"sourcelists. This is recommended to avoid errors if the repo directory "
"changes or is removed. Always perform this step before exiting the program."
"\\n\\n\\nNote: search and install options can be changed by clicking the "
"'Configuration Options' (gear icon) button. Each option is described in the "
"HELP window launched from inside the 'CONFIGURATION' window."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:295
msgid ""
" <i>changes to these settings take effect when you click <b>Apply</b> "
"button</i>\\n\\n<b>Default Download Folder</b>:\\n    recalled each time you "
"launch the 'Download Programs' window\\n\\nAPT Search Match\\n    <b>normal</"
"b>: attempts to match ALL words entered for search\\n    <b>strict</b>: "
"search for packages that <i>start with</i> the word entered\\n    <b>exact</"
"b>: search for an exact package name\\n\\nGeneral Search Match\\n    Same "
"options as with 'APT Search Match', but for searching PI and .Repo files.\\n"
"\\nRetrieve Dependencies\\n    <b>minimum</b>: only download the <i>direct</"
"i> dependencies listed for the package\\n    <b>2-levels</b>: also download "
"any direct dependencies of the dependencies\\n    <b>compromise</b>: tries "
"to compromise between ALL dependencies and 2-levels. Should be enough for "
"most cases.\\n    <b>ALL</b>: download all known dependencies, as specified "
"by apt-cache.\\n\\n[*] <b>Use exclude list</b>\\n    If ticked, will ignore "
"all packages listed in the exclude.list file.Most of them are (must be) "
"already installed on any debian//antiX system. Should reduce the amount of "
"packages downloaded. Good for saving bandwidth and disk space.\\n\\n[*] "
"<b>While downloading, remove older package versions</b>\\n    If you don't "
"want to keep different versions of the same package/program, select this "
"option. If this option is active, next time a package is downloaded it will "
"remove all other version previously downloaded. This will save a lot of "
"space if you keep the same local repo file for a long time.\\n\n"
"If you DO want to keep older versions of packages for compatibility reason, "
"keep this option disabled (Un-tick)."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:322
msgid ""
" <i>changes to these setting take effect when you click <b>Apply</b> button</"
"i>\\n\\n<b>Default Download Folder</b>:\\n    recalled each time you launch "
"the 'Download Updates' window\\n\\n[*] <b>While downloading, remove older "
"package versions</b>\\n    If you don't want to keep different versions of "
"the same package/program, select this option. If this option is active, next "
"time a package is downloaded it will remove all other version previously "
"downloaded. This will save a lot of space if you keep the same local repo "
"folder for a long time.\\n\n"
"If you DO want to keep older versions of packages for compatibility reason, "
"keep this option disabled (Un-tick).\\n\\n[*] <b>Download new dependencies "
"with updates (experimental)</b>\\n    If the option is active, and a new "
"update requires a new extra package to work, it will add it to the update "
"list.\\nAs previously mentioned, <b>this option is experimental</b>. It may "
"list an incorrect number of new dependencies, or even less than the ones "
"needed for the new update to work. It may be slow and inconsistent."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:339
msgid ""
" <i>changes to these setting take effect when you click <b>Apply</b> button</"
"i>\\n\\n<b>Default Repo Folder</b>:\\n    recalled each time you launch the "
"'Offline Repo Installer' window\\n    ( convenient when performing multiple "
"installations )\\n\\nAPT Search Match\\n    <b>normal</b>: attempts to match "
"ALL words entered for search\\n    <b>strict</b>: search for packages that "
"<i>start with</i> the word entered\\n    <b>exact</b>: search for an exact "
"package name\\n\\nGeneral Search Match\\n    Same options as with APT Search "
"Match, but searching Package Installer and .Repo files.\\n\\n[*] <b>Auto "
"clean the local sources list</b>\\n    If this option is active, the script "
"will make sure to remove the local repo from sourceslist every time you exit "
"the 'Offline Repo Installer' window. This option was created to avoid the "
"effects of forgetting to click the 'Remove repo from list' button, which "
"causes a lot of trouble if you don't know how to fix it."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:356
msgid ""
"my APT Offline Repository\\n\\n   <b>This program enables you to download/"
"install .deb packages \\n   and to create+manage an offline (local) package "
"repository.</b>\\n\\n   project homepage: https://gitlab.com/nXecure/my-"
"offline-repo \\n\\nSelect <b>Download a program + its dependencies</b> to "
"download packages into the folder you want to use as your offline repo.\\n"
"\\nSelect <b>Download Updates</b> to check for any available updates for an "
"offline system and download them for you to a local folder.\\n\\nSelect "
"<b>Install from this local repo</b> to specify which directory will serve as "
"your offline repo storage location and to install programs from debfiles "
"currently stored there.\\n\\n"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:368
msgid "HELP"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:369
msgid "<b><big>Manage your local offline repo</big></b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:426
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:443
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:455
msgid "CONFIGURATION"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:428
msgid "<b><big>Configuration for Downloading dependencies</big></b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:429
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:446
msgid "Default Download Folder"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:430
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:459
msgid "APT Search match"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:431
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:460
msgid "General Search match"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:432
msgid "Retrieve Dependencies"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:433
msgid "Use exclude list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:435
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:447
msgid "While downloading, remove older package versions"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:436
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:449
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:463
msgid "gtk-apply!!Save changes"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:445
msgid "<b><big>Configuration for Downloading Updates</big></b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:448
msgid "Download new dependencies with updates (experimental)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:457
msgid "<b><big>Configuration for Installing from local repo</big></b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:458
msgid "Default Repo Folder"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:462
msgid "Auto clean the local sources list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:475
msgid "General Error"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:479
msgid "Directory not found"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:480
msgid "\\n <b>$ERR_SUBJECT</b> is NOT a valid path."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:482
msgid "No Sourcefile"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:483
msgid "\\n <b>$ERR_SUBJECT</b> needs to exist for the Offline repo to work."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:485
msgid "Not valid Repo"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:486
msgid "\\n Your folder<b>$ERR_SUBJECT</b> isn't a valid repo folder."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:488
msgid "Package not found"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:489
msgid "\\n <b>$ERR_SUBJECT</b> is NOT a valid package name."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:491
msgid "Package not processed"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:492
msgid "\\n Package <b>$ERR_SUBJECT</b> cannot be processed. Contact dev."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:494
msgid "Package not selected"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:495
msgid "\\n <b>NO package</b> was selected."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:497
msgid "Incorrect Password"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:498
msgid "\\n <b>SUDO password</b> is wrong."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:500
msgid "APT is locked"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:501
msgid "\\n <b>APT is locked</b> by another program."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:503
msgid "Invalid option"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:504
msgid "\\n <b>$ERR_SUBJECT</b> is not a valid option."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:506
msgid "NO packagelist files"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:507
msgid "\\n <b>No .packagelist</b> files were found in\\n $ERR_SUBJECT"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:509
msgid "Possible exclusion"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:510
msgid ""
"\\n Required packages \\n<b>$ERR_SUBJECT</b> \\n were not added to the "
"dependency list.\\nThey may have possibly been excluded. \\nPlease revise or "
"disable the exclude.list file."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:513
msgid "Non Valid packagelist"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:514
msgid "\\n <b>$ERR_SUBJECT</b> is not a valid packagelist."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:590
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1120
msgid "Package list (items slated for downloading)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:591
msgid "List of packages ready to update (with sources and architecture)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:611
msgid "# Getting URL for ${PACKAGE_NAME%:*} from $PACKAGE_SOURCE"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:620
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:623
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1146
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1149
msgid "URL list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:647
msgid "Fetching a list of all packages available"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:654
msgid "Fetching all packages available"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:655
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:742
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:743
msgid "Checking for updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:664
msgid ""
"\\nThere are no new updates available right now for\\n<b>$PACKAGE_LIST_NAME</"
"b>."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:665
msgid "No Updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:697
msgid "# Checking for updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:754
msgid "Total updates for $PACKAGE_LIST_NAME: <b>$TOTAL_UPDATES</b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:759
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:798
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:859
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:860
msgid "Checking for extra-updates"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:851
msgid "new from $REVERSE_DEPENDS"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:867
msgid ""
"${TEXT_UPDATES} \\nNew packages for $PACKAGE_LIST_NAME: <b>$NEW_UPDATES</b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:873
msgid "List of Updates to download"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:874
msgid "$TEXT_UPDATES \\nList of Updates ready to be downloaded"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:875
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1726
msgid "Package"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:876
msgid "Source"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:876
msgid "Original"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:876
msgid "Update"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:877
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:970
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1460
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1558
msgid "LIST"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:878
msgid "Download Updates!gtk-save!Download all updates listed"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:918
msgid "# Downloading ${PACKAGE_NAME%:*}"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:936
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1234
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1520
msgid "Downloading..."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:941
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1525
msgid "# Updating the local package list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:946
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1530
msgid "Checking packages"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:953
msgid ""
"All <b>$TOTAL_UPDATES</b> updates for <b>$PACKAGE_LIST_NAME</b> were "
"downloaded.\\n\\n Your local Repo packagelist has been updated."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:955
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1543
msgid "Packagelist created"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:966
msgid ""
"Some updates for <b>$PACKAGE_LIST_NAME</b>\\n could not be saved.\\n"
"\\nPossible reasons: The download couldn't start, or was stopped abruptly."
"\\nClick the <b>List</b> button to review the list of failed//skipped "
"updates.\\n\\nYour local Repo packagelist has been updated."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:971
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1559
msgid "Error Downloading"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
msgid "Download from file"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
msgid ""
"Select a file containing a list of packages\\n and Download them (+ "
"dependencies)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
msgid "Package List file"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
msgid "List Name"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1726
msgid "Description"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1007
msgid ""
"Download!gtk-goto-bottom!Download packages and dependencies from list in file"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1104
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1766
msgid "# Updating package list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1108
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1295
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1772
msgid "Updating packagelist"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1109
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1296
msgid "Cleanup"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1121
msgid ""
"Depending on your my-offline-repo configuration choices, the list of "
"packages to be downloaded may not include the ones listed in the exclude."
"list file (or its dependencies).\\n\\nIf you want to see and download all "
"packages without filtering, UNTICK the option 'Use exclude list' in "
"Configuration."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1137
msgid "# Getting URL for $line"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1173
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1244
msgid "# Building dependency list for $word"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1177
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1178
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1248
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1249
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1349
#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1350
msgid "Getting Dependencies"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1231
msgid "# Downloading $PACKAGE_FILE"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1235
msgid "Downloading $PACKAGE_FILE"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1291
msgid "# Adding sources list and updating package list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1345
msgid "# Building dependency list for $PACKAGE_LINE"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1454
msgid "Downloading $PACKAGE_NAME"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1456
msgid "\\nPackage selection: <b>$PACKAGE_NAME</b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1457
msgid "Repo folder: <b>${D_DIRECTORY}</b> (${SELECTED_ARCH})"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1458
msgid "Total packages: <b>$TOTAL_PACKAGES</b>"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1459
msgid "Do you want to download them?"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1502
msgid "# Downloading $line"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1540
msgid ""
"<b>$PACKAGE_NAME</b> and its dependencies were downloaded.\\n\\n Your local "
"Repo packagelist has been updated.\\nA new file <b>${PACKAGE_NAME%:*}_"
"${SYSTEM_ARCH}.repo</b> was created."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1554
msgid ""
"<b>$PACKAGE_NAME</b>\\n and/or some of its dependencies could not be saved."
"\\n\\nPossible reasons: The download couldn't start, or was stopped abruptly."
"\\nClick the <b>List</b> button to review the list of failed//skipped "
"packages.\\n\\nYour local Repo packagelist has been updated."
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1604
msgid "# Searching antiX package installer (PI)"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1653
msgid "# Searching .repo files"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1712
msgid "# Searching APT list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1721
msgid "Searching for packages"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1722
msgid "Searching"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1726
msgid "Select Package"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1726
msgid ""
"Search parameters: <b>$PACKAGE_NAME</b>\\nSelect a package from the list"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1753
msgid "apt update"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1790
msgid "install $PACKAGE_NAME"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1840
msgid "Missing dependencies"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1842
msgid "the 'dpkg-dev' utilities are not currently installed on your system"
msgstr ""

#: /home/pc/gitz/my-offline-repo/my-offline-repo.sh:1843
msgid "Do you want to install them? (2MB)"
msgstr ""
