# my Offline repo

my Offline repo is a bash script specially designed to help users download, install and update Debian packages for their offline Debian based systems.

It is a much slower alternative to apt-offline, and lacks a command line interface (it only works on GUI environments), but is a bit more user-friendly and gives the possibility to "easily" download new packages for the offline system.

----

## Dependencies:

apt, apt-cache, yad, sudo and dpkg-dev (only really needed in the online system for downloading packages and building the package list of downloaded .deb files).

## Overview

Download packages to a local folder (let us name it "Repo folder"), build a package list of the downloaded programs, and use the Repo folder to install them on any system easily with APT. 

my Offline repo lets you:

1. Download new packages and its dependencies for later installation.
2. Download updates for your system or for a different (offline) system for later installation.
3. Install new programs/packages and Updates on a different system from a "local repository".

## Downloading programs + dependencies
