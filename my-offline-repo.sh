#!/bin/bash

###  my (APT) Offline Repository    
#                  version 0.11.7
###  bash-yad application to download/install .deb packages and manage
###  an offline (local) package repository
###  project homepage:  https://gitlab.com/nXecure/my-offline-repo
###  contributions by skidoo

###  dependencies: apt, apt-cache, yad, sudo (dpkg-dev, for download)

###       Warning: this program uses up to 130 MB of RAM

########################################################################
##########  LOAD/SET OPTIONS AND PREPARE LOCALE TRANSLATIONS  ##########
########################################################################

### Folder where the script is stored
SCRIPT_FOLDER="$(dirname "$(readlink -f "$0")")"

### Translation
TEXTDOMAINDIR="${SCRIPT_FOLDER}/locale"
TEXTDOMAIN=my-offline-repo

### Create tempfiles
PI_CONTENT=$(mktemp)
MOR_CONF=$(mktemp)
EXIT_SCRIPT=$(mktemp)
### Exit script after closing window
echo "true" > $EXIT_SCRIPT

#### Check System architecture ####
if [ "$(uname -a | grep -Ec 'i386|i486|i586|i686')" -eq "1" ]; then
	SYSTEM_ARCH="i386"
else
	SYSTEM_ARCH="amd64"
fi
echo "OS architecture: $SYSTEM_ARCH"

### Some YAD text variables
YAD_FINISHED=$"\n Finished"
THIS_MACHINE=$"This Machine"

########################################################################
#######################  1. MAIN DIALOG WINDOWS  #######################
########################################################################

#### MAIN WINDOW TO ACCESS OTHER WINDOWS ####
main_dialog(){
	YAD_TITLE=$"My OFFLINE repo"
	export YAD_TITLE=$YAD_TITLE
	yad --window-icon=synaptic --title="$YAD_TITLE" \
	--class="apt-offline-repo" --name="APT-Offline-Repo" --center \
	--form --borders=10 --align=center --separator="" --width=480 \
	--field=$"OS architecture: <b>$SYSTEM_ARCH</b>\n":LBL '' \
	--field=$"Download a program + its dependencies":FBTN 'bash -c "echo \"false\" > $EXIT_SCRIPT; \
		download_dialog & kill -USR1 $YAD_PID"' \
	--field=$"Download Updates":FBTN 'bash -c "echo \"false\" > $EXIT_SCRIPT; \
		update_dialog & kill -USR1 $YAD_PID"' \
	--field=$"Install from this local repository":FBTN 'bash -c "echo \"false\" > $EXIT_SCRIPT; \
		repo_dialog & kill -USR1 $YAD_PID"' \
	--button='gtk-help':'bash -c "help_dialog"' --button='gtk-quit':1
	local exitcode=$?
	if [ $exitcode -eq 1 ] || [ $exitcode -eq 252 ]; then
		echo "true" > $EXIT_SCRIPT
	fi
}

#### WINDOW TO DOWNLOAD PACKAGES AND DEPENDENCIES ####
download_dialog(){
	echo "Download Programs Dialog"
	### Load Default Download Directory from Configuration file
	DOWNLOAD_DIR="$(check_config "LOAD" "DOWNLOAD_DIR")"
	yad --window-icon=synaptic --title=$"Download Programs" \
	--class="apt-offline-repo" --name="Apt-Download" --center --text-align="center" \
	--text=$"Select the program you want to download \nand where to save it.\n" \
	--form --borders=10 --columns=3 --focus-field=6 \
	--field=$"Folder":LBL '' --field=$"Package Name":LBL '' \
	--field=$"Select file!!Select a file containing a collection of packages you want to download":FBTN 'bash -c "download_dependencies %5 select_file"' \
	--field=" ":LBL '' --field="":DIR "$DOWNLOAD_DIR" --field="":CBE "" \
	--field=$"Update package list!gtk-convert!Update package list (sudo apt update)":FBTN 'bash -c "update_apt"' \
	--field=" ":LBL '' --field=" ":LBL '' \
	--field=$"!gtk-find!Search for selected package":FBTN 'bash -c "download_dependencies %5 %6"' \
	--field=$"!gtk-preferences!Configure Options":FBTN 'bash -c "conf_dialog Download"' --field=" ":LBL '' \
	--button=$"!gtk-go-back-ltr":'bash -c "main_dialog & kill -USR2 $YAD_PID"' \
	--button='gtk-help':'bash -c "help_dialog \"Download\""'
	local exitcode=$?
	if [ $exitcode -eq 0 ] || [ $exitcode -eq 252 ]; then
		echo "true" > $EXIT_SCRIPT
	fi
}

#### WINDOW TO DOWNLOAD UPDATES FOR AN OFFLINE SYSTEM ####
update_dialog(){
	### Check if packagelists folder exists and move into it
	local PACKAGELISTS_FOLDER="${SCRIPT_FOLDER}/packagelists"
	if [ -d "$PACKAGELISTS_FOLDER" ]; then
		if [ $(ls -lh "$PACKAGELISTS_FOLDER" | grep -ic ".packagelist") -eq 0 ]; then
			error_dialog "NO PACKAGELISTS" "$PACKAGELISTS_FOLDER"
		fi
	else
		mkdir "$PACKAGELISTS_FOLDER"
		error_dialog "NO PACKAGELISTS" "$PACKAGELISTS_FOLDER"
	fi
	export PACKAGELISTS_FOLDER
	cd "$PACKAGELISTS_FOLDER"
	
	### Build list of .packagelist files available
	local LIST_FILES="$THIS_MACHINE"
	for file in *.packagelist; do
		if [[ "$file" == *"${SYSTEM_ARCH}.packagelist" ]]; then
			if [ -n "$LIST_FILES" ]; then
				LIST_FILES="${LIST_FILES}!${file%_*}"
			else
				LIST_FILES="${file%_*}"
			fi
		fi
	done
	
	### Function to create packagelist for the current system
	function add_packagelist(){
		NEW_PACKAGE_NAME=$(yad --window-icon=synaptic \
			--title=$"Create packagelist" --class="apt-offline-repo" --name="New-Packagelist" \
			--borders=10 --center --text-align="center" --separator="" --width=460 \
			--text=$"Give a name to the new packagelist you want to create.\n\
It will contain the list of packages installed\n on the current machine.\n\n" \
			--form --field=$"PackageList Name":CE "" \
			--button=$"Save packagelist!gtk-save!Save the list of packages on this machine":0 \
			--button='gtk-cancel':1)
			local exitcode=$?
			if [ $exitcode -eq 0 ] && [ ! -z "$NEW_PACKAGE_NAME" ]; then
				local YAD_TEXT
				local NEW_PACKAGE_LIST
				
				## New packagelist name
				NEW_PACKAGE_NAME="$(echo "$NEW_PACKAGE_NAME" | sed 's/ /-/g')"
				NEW_PACKAGE_LIST="${NEW_PACKAGE_NAME}_${SYSTEM_ARCH}.packagelist"
				
				### Check if a file with the same name already exists
				if [[ -f "${PACKAGELISTS_FOLDER}/${NEW_PACKAGE_LIST}" ]]; then
					YAD_TEXT=$"There already is a file with the name\n $NEW_PACKAGE_LIST \n NO new packagelist created."
				else
					apt --installed list | tail -n+2 | cut -d "[" -f 1 | sed 's/,now / /g' | \
					sed 's/\/now /\/ /g' > "${PACKAGELISTS_FOLDER}/${NEW_PACKAGE_LIST}"
					YAD_TEXT=$"A new packagelist named ${NEW_PACKAGE_LIST} was saved. \n\
You can verify it by relaunching the current Updates window."
				fi
				
				### Inform the user of the result
				yad --center --width=350 --text-align=center \
				--text="$YAD_TEXT" --margins=5 \
				--button='gtk-close':1 --title=$"Packagelist creation" --borders=5 \
				--window-icon=gtk-dialog-info --image="gtk-dialog-info"
			fi
	}
	export -f add_packagelist
	
	### Load Default Download Directory from Configuration file
	DOWNLOAD_DIR="$(check_config "LOAD" "DOWNLOAD_DIR")"
	
	### Load Download updates Dialog
	echo "Download Updates Dialog"
	yad --window-icon=synaptic --title=$"Download Updates" \
	--class="apt-offline-repo" --name="Update-Download" --center --text-align="center" \
	--text=$"Select a System and a folder to save any packages.\nCheck for updates and download them.\n" \
	--form --borders=10 --columns=3 --focus-field=6 \
	--field=$"Folder":LBL '' --field=$"Package List":LBL '' \
	--field=$"New Packagelist!!Create a new .packagelist file specifically for this machine":FBTN 'bash -c "add_packagelist"' \
	--field=" ":LBL '' --field="":DIR "$DOWNLOAD_DIR" --field="":CB "$LIST_FILES" \
	--field=$"Update package list!gtk-convert!Update package list (sudo apt update)":FBTN 'bash -c "update_apt"' \
	--field=" ":LBL '' --field=" ":LBL '' \
	--field=$"Check!gtk-apply!Check for updates for the selected package list and download them":FBTN 'bash -c "download_updates %5 %6"' \
	--field=$"!gtk-preferences!Configure Options":FBTN 'bash -c "conf_dialog Update"' --field=" ":LBL '' \
	--button=$"!gtk-go-back-ltr":'bash -c "main_dialog & kill -USR2 $YAD_PID"' \
	--button='gtk-help':'bash -c "help_dialog \"Update\""'
	local exitcode=$?
	if [ $exitcode -eq 0 ] || [ $exitcode -eq 252 ]; then
		echo "true" > $EXIT_SCRIPT
	fi
}

#### WINDOW TO SET UP OFFLINE REPO ####
repo_dialog(){
	echo "Install from Repo Dialog"
	### Load Default Repo Directory from Configuration file
	INSTALL_DIR="$(check_config "LOAD" "INSTALL_DIR")"
	
	### Function to install updates easily
	install_updates(){
		echo "Installing updates."
		x-terminal-emulator -T "apt upgrade" -e /bin/bash -c \
			"sudo apt upgrade && sleep 0.1 &&
			yad --center --width=250 --text-align=center --text=\"$YAD_FINISHED\" \
			--button='gtk-ok':1 --title='apt - Upgrade' "
	}
	export -f install_updates
	### Yad window
	yad --window-icon=synaptic --title=$"Offline Repo Installer" \
	--class="apt-offline-repo" --name="dpkg-repo" --center --text-align="center" \
	--text=$"Select the folder where your repo is stored. Add repo.\
\nSearch for a program to install from repo.\
\nRemove repo from sourcelist when finished.\n" \
	--form --borders=10 --columns=3 \
	--field=$"Repo Folder":LBL '' --field=$"Package Name":LBL '' \
	--field=$"Upgrade!!Install all available updates":FBTN 'bash -c install_updates' \
	--field=" ":LBL '' --field="":DIR "$INSTALL_DIR" --field="":CBE "" \
	--field=$"Remove repo from list!gtk-remove!REMEMBER to ALWAYS remove the repo \
from the sourcelist before exiting the program":FBTN 'bash -c "repo_update %5 1"' \
	--field=" ":LBL '' --field=$"Add repo!gtk-add!Add this folder as an offline repo \
to the sources list and update packagelist":FBTN 'bash -c "repo_update %5 0"' \
	--field=$"Search!gtk-find!Search for the package you want to install. \
Leave empty to display all options.":FBTN 'bash -c "search_install %5 %6"' \
	--field=$"!gtk-preferences!Configure Options":FBTN 'bash -c "conf_dialog Install"' \
	--button=$"!gtk-go-back-ltr":'bash -c "main_dialog & kill -USR2 $YAD_PID"' \
	--button='gtk-help':'bash -c "help_dialog \"Repo\""'
	local exitcode=$?
	if [ $exitcode -eq 0 ] || [ $exitcode -eq 252 ]; then
		echo "true" > $EXIT_SCRIPT
	fi
	### AUTO_CLEAN cleans repo is active
	D_AUTO_CLEAN="$(check_config "LOAD" "AUTO_CLEAN")"
	if [[ "${D_AUTO_CLEAN,,}" == "true" ]]; then
		repo_update " " 1
	fi
}

#### HELP WINDOW ####
help_dialog(){
	local HELP_D="${1}"
	local HELP_TEXT
	
	### HELP for Download Dependencies Dialog
	if [[ "$HELP_D" == "Download" ]]; then
		HELP_TEXT=$"<b>Download Programs</b> will let you download any package \
(along with all its dependencies) and will update the local repo information. Follow these steps:\n\n\
<b>1. Update package list</b> to ensure you are downloading the newest programs.\n\n\
<b>2.</b> Select the <b>Folder</b> where you want programs and their dependencies \
to be downloaded and saved. This folder will become your Repo folder.\n\n\
<b>3A. </b>Write the <b>Package Name</b> you want to add to your local repo. \
If you don't know the exact name, use as few descriptive words as possible (example chrome for google-chrome).\n\n\
<b>3B. If you already have a file with a list of packages you want to download, \
click the </b>Select file</b> button to download packages listed in that file. \
You can optionally give this selection a Name and Description (easier to find later).\n\n\
<b>4. Search</b> (search button beside the Package Name entry) will search \
for the name (or the words) you wrote in <b>Package Name</b> and will give you a list of results.\n\n\
<b>5.</b> In the popup search results window, select the package you want to download \
and it will start building the dependency list. \
A confirmation window will display information about the program/package selected and the amount \
of dependencies it will download (You can check them clicking the <b>LIST</b> button). \
If you agree, the download process will begin.\n\n\n\
Note: download options can be changed by clicking the 'Configuration Options' (gear icon) button. \
Each option is described in the HELP window launched from inside the 'CONFIGURATION' window."
	elif [[ "$HELP_D" == "Update" ]]; then
		HELP_TEXT=$"<b>Download Updates</b> will let you check for any available updates \
for an offline system and download them for you to a local folder.\n\n\
Package information for any offline system should be stored in a <b>.packagelist file</b> inside the \
<i>packagelists</i> folder. You can either use the already created ones there or create your own \
based on the installed packages on your offline system. For that, you will need to launch this program \
on the offline machine and click the <b>New Packagelist</b> button to create the new packagelist.\n\
Standard packagelists for antiX 19.3 vanilla systems have already been created.\n\n\
To check for updates, you need to follow these steps:\n\
<b>1. Update package list</b> to ensure you are checking for the newest updates.\n\n\
<b>2.</b> Select the <b>Folder</b> where you want your updates \
to be downloaded and saved. This folder will become your Repo folder.\n\n\
<b>3.</b> Select the <b>Package List</b> that is related to the system which you want to update. \
If there are no available options, it means that no .packagelist files can be found inside \
the packagelists folder.\n\n\
<b>4. Check</b> button will check for any updates for the Package List selected. \
If any updates are found, a new window will display a list of update. \
You can then choose to Download them or cancel the download.\n\n\n\
Note: download options can be changed by clicking the 'Configuration Options' (gear icon) button. \
Each option is described in the HELP window launched from inside the 'CONFIGURATION' window."
	### HELP for Install from Repo Dialog
	elif [[ "$HELP_D" == "Repo" ]]; then
		HELP_TEXT=$"<b>Offline Repo Installer</b> will let you add a folder containing \
a collection of packages to your sources lists, acting as an offline local repo.\n\n\
<b>1.</b> Select the <b>Repo Folder</b> that contains your downloaded packages and dependencies.\n\n\
<b>2.</b> Click the <b>Add repo</b> button (beside the <b>Repo Folder</b> entry) \
to add your repo directory to the apt sources lists.\n\n\
<b>3A.</b> Write the <b>Package Name</b> you want to install. Leave empty if you want \
to see all available programs from the local Repo.\n\n\
<b>3B.</b> Click the <b>Upgrade</b> button if you only want to install updates on your offline system. \
It should launch a new terminal window displaying all updates available.\n\n\
<b>4. Search</b> for all available packages that match the selected <b>Package Name</b>. \
If there are .repo files in the repo folder, it will display them at the top of the search results. \
Install the package selected on your current system.\n\n\
<b>5. Remove repo from list</b> will delete all local repos from \
your sourcelists. This is recommended to avoid errors if the repo directory \
changes or is removed. Always perform this step before exiting the program.\n\n\n\
Note: search and install options can be changed by clicking the 'Configuration Options' (gear icon) button. \
Each option is described in the HELP window launched from inside the 'CONFIGURATION' window."

	### HELP for Download CONFIGURATION Dialog
	elif [[ "$HELP_D" == "Download_Config_Help" ]]; then
		HELP_TEXT=$" <i>changes to these settings take effect when you click <b>Apply</b> button</i>\n\n\
<b>Default Download Folder</b>:\n\
    recalled each time you launch the 'Download Programs' window\n\n\
APT Search Match\n\
    <b>normal</b>: attempts to match ALL words entered for search\n\
    <b>strict</b>: search for packages that <i>start with</i> the word entered\n\
    <b>exact</b>: search for an exact package name\n\n\
General Search Match\n\
    Same options as with 'APT Search Match', but for searching PI and .Repo files.\n\n\
Retrieve Dependencies\n\
    <b>minimum</b>: only download the <i>direct</i> dependencies listed for the package\n\
    <b>2-levels</b>: also download any direct dependencies of the dependencies\n\
    <b>compromise</b>: tries to compromise between ALL dependencies and 2-levels. Should be enough for most cases.\n\
    <b>ALL</b>: download all known dependencies, as specified by apt-cache.\n\n\
[*] <b>Use exclude list</b>\n\
    If ticked, will ignore all packages listed in the exclude.list file.\
Most of them are (must be) already installed on any debian//antiX system. \
Should reduce the amount of packages downloaded. Good for saving bandwidth and disk space.\n\n\
[*] <b>While downloading, remove older package versions</b>\n\
    If you don't want to keep different versions of the same package/program, select this option. \
If this option is active, next time a package is downloaded it will remove all other version previously \
downloaded. This will save a lot of space if you keep the same local repo file for a long time.\n
If you DO want to keep older versions of packages for compatibility reason, \
keep this option disabled (Un-tick)."

	### HELP for Update from CONFIGURATION Dialog
	elif [[ "$HELP_D" == "Update_Config_Help" ]]; then
		HELP_TEXT=$" <i>changes to these setting take effect when you click <b>Apply</b> button</i>\n\n\
<b>Default Download Folder</b>:\n\
    recalled each time you launch the 'Download Updates' window\n\n\
[*] <b>While downloading, remove older package versions</b>\n\
    If you don't want to keep different versions of the same package/program, select this option. \
If this option is active, next time a package is downloaded it will remove all other version previously \
downloaded. This will save a lot of space if you keep the same local repo folder for a long time.\n
If you DO want to keep older versions of packages for compatibility reason, \
keep this option disabled (Un-tick).\n\n\
[*] <b>Download new dependencies with updates (experimental)</b>\n\
    If the option is active, and a new update requires a new extra package to work, it will add it to the update list.\n\
As previously mentioned, <b>this option is experimental</b>. It may list an incorrect number of new dependencies, or even \
less than the ones needed for the new update to work. \
It may be slow and inconsistent."

	### HELP for Install from CONFIGURATION Dialog
	elif [[ "$HELP_D" == "Install_Config_Help" ]]; then
		HELP_TEXT=$" <i>changes to these setting take effect when you click <b>Apply</b> button</i>\n\n\
<b>Default Repo Folder</b>:\n\
    recalled each time you launch the 'Offline Repo Installer' window\n\
    ( convenient when performing multiple installations )\n\n\
APT Search Match\n\
    <b>normal</b>: attempts to match ALL words entered for search\n\
    <b>strict</b>: search for packages that <i>start with</i> the word entered\n\
    <b>exact</b>: search for an exact package name\n\n\
General Search Match\n\
    Same options as with APT Search Match, but searching Package Installer and .Repo files.\n\n\
[*] <b>Auto clean the local sources list</b>\n\
    If this option is active, the script will make sure to remove the local repo from sourceslist \
every time you exit the 'Offline Repo Installer' window. \
This option was created to avoid the effects of forgetting to click the 'Remove repo from list' \
button, which causes a lot of trouble if you don't know how to fix it."
	### General help
	else
		HELP_TEXT=$"my APT Offline Repository\n\n\
   <b>This program enables you to download/install .deb packages \n\
   and to create+manage an offline (local) package repository.</b>\n\n\
   project homepage: https://gitlab.com/nXecure/my-offline-repo \n\n\
Select <b>Download a program + its dependencies</b> to \
download packages into the folder you want to use as your offline repo.\n\n\
Select <b>Download Updates</b> to check for any available updates \
for an offline system and download them for you to a local folder.\n\n\
Select <b>Install from this local repo</b> to specify which directory will serve as your offline repo storage location \
and to install programs from debfiles currently stored there.\n\n"
	fi
	yad --window-icon=gtk-dialog-info --image="gtk-dialog-info" --height=440 --width=640 --scroll \
	--title=$"HELP" --class="apt-offline-repo" --name="Help" --borders=10 --center \
	--text=$"<b><big>Manage your local offline repo</big></b>" \
	--form --field="":LBL '' --field="$HELP_TEXT":LBL '' --field=" ":LBL '' \
	--separator="" --button=gtk-close:0 --buttons-layout=center
}

#### CONFIGURATION DIALOG ####
conf_dialog(){
	local WHAT_WINDOW="${1}"
	
	### Load current/Default options
	D_DOWNLOAD_DIR="$(check_config "LOAD" "DOWNLOAD_DIR")"
	D_INSTALL_DIR="$(check_config "LOAD" "INSTALL_DIR")"
	D_DEPENDENCY_MODE="$(check_config "LOAD" "DEPENDENCY_MODE")"
	D_EXCLUDE_LIST="$(check_config "LOAD" "EXCLUDE_LIST")"
	D_APT_MATCH="$(check_config "LOAD" "APT_MATCH")"
	D_SEARCH_MATCH="$(check_config "LOAD" "SEARCH_MATCH")"
	D_REMOVE_OLD="$(check_config "LOAD" "REMOVE_OLD")"
	D_AUTO_CLEAN="$(check_config "LOAD" "AUTO_CLEAN")"
	D_UPDATE_EXTRA="$(check_config "LOAD" "UPDATE_EXTRA")"
	### Default list
	LIST_MATCH="normal strict exact"
	LIST_DEPENDENCY="minimum 2-levels compromise ALL"
	### Build list of options, starting with current
	LIST_APT_MATCH="$D_APT_MATCH"
	LIST_SEARCH_MATCH="$D_SEARCH_MATCH"
	LIST_DEPENDENCY_MODE="$D_DEPENDENCY_MODE"
	for word in $LIST_MATCH; do
		if [ $(echo "$word" | grep -ic "$D_APT_MATCH") -eq 0 ]; then
			LIST_APT_MATCH="${LIST_APT_MATCH}!${word}"
		fi
		if [ $(echo "$word" | grep -ic "$D_SEARCH_MATCH") -eq 0 ]; then
			LIST_SEARCH_MATCH="${LIST_SEARCH_MATCH}!${word}"
		fi
	done
	for word in $LIST_DEPENDENCY; do
		if [ $(echo "$word" | grep -ic "$D_DEPENDENCY_MODE") -eq 0 ]; then
			LIST_DEPENDENCY_MODE="${LIST_DEPENDENCY_MODE}!${word}"
		fi
	done
	
	### subfunction to store changes to the config file
	save_config(){
		local CONFIG_VAR="${1}"
		local CONF_VALUE="${2}"
		### Get current default value:
		local D_CONF_VAL="$(check_config "LOAD" "$CONFIG_VAR")"
		### Only save value if the value is different from default
		if [[ "${D_CONF_VAL,,}" != "${CONF_VALUE,,}" ]]; then
			echo "Saved $CONFIG_VAR as $CONF_VALUE"
			check_config "SAVE" "$CONFIG_VAR" "$CONF_VALUE"
		fi
	}
	export -f save_config
	
	### Select what window to load
	if [[ "$WHAT_WINDOW" == "Download" ]]; then
		yad --window-icon=gtk-preferences \
		--title=$"CONFIGURATION" --class="apt-offline-repo" --name="Configuration" \
		--borders=10 --center --text-align="center" --width=460 \
		--text=$"<b><big>Configuration for Downloading dependencies</big></b>" \
		--form --field="":LBL '' --field=$"Default Download Folder":DIR "$D_DOWNLOAD_DIR" \
		--field=$"APT Search match":CB "$LIST_APT_MATCH" \
		--field=$"General Search match":CB "$LIST_SEARCH_MATCH" \
		--field=$"Retrieve Dependencies":CB "$LIST_DEPENDENCY_MODE" \
		--field=$"Use exclude list":CHK "$D_EXCLUDE_LIST" \
		--field=$"":LBL '' \
		--field=$"While downloading, remove older package versions":CHK "$D_REMOVE_OLD" \
		--field=$"gtk-apply!!Save changes":FBTN 'bash -c "save_config \"DOWNLOAD_DIR\" %2; \
			save_config \"APT_MATCH\" %3; save_config \"SEARCH_MATCH\" %4; \
			save_config \"DEPENDENCY_MODE\" %5; save_config \"EXCLUDE_LIST\" %6; \
			save_config \"REMOVE_OLD\" %8"' \
		--button=$'gtk-help':'bash -c "help_dialog \"Download_Config_Help\"" ' --button=gtk-close:1
	elif [[ "$WHAT_WINDOW" == "Update" ]]; then
		yad --window-icon=gtk-preferences \
		--title=$"CONFIGURATION" --class="apt-offline-repo" --name="Configuration" \
		--borders=10 --center --text-align="center" --width=460 \
		--text=$"<b><big>Configuration for Downloading Updates</big></b>" \
		--form --field="":LBL '' --field=$"Default Download Folder":DIR "$D_DOWNLOAD_DIR" \
		--field=$"While downloading, remove older package versions":CHK "$D_REMOVE_OLD" \
		--field=$"Download new dependencies with updates (experimental)":CHK "$D_UPDATE_EXTRA" \
		--field=$"gtk-apply!!Save changes":FBTN 'bash -c "save_config \"DOWNLOAD_DIR\" %2; \
				save_config \"REMOVE_OLD\" %3; save_config \"UPDATE_EXTRA\" %4" ' \
		--button=$'gtk-help':'bash -c "help_dialog \"Update_Config_Help\"" ' \
		--button=gtk-close:1
	elif [[ "$WHAT_WINDOW" == "Install" ]]; then
		yad --window-icon=gtk-preferences \
		--title=$"CONFIGURATION" --class="apt-offline-repo" --name="Configuration" \
		--borders=10 --center --text-align="center" --width=460 \
		--text=$"<b><big>Configuration for Installing from local repo</big></b>" \
		--form --field="":LBL '' --field=$"Default Repo Folder":DIR "$D_INSTALL_DIR" \
		--field=$"APT Search match":CB "$LIST_APT_MATCH" \
		--field=$"General Search match":CB "$LIST_SEARCH_MATCH" \
		--field=$"":LBL '' \
		--field=$"Auto clean the local sources list":CHK "$D_AUTO_CLEAN" \
		--field=$"gtk-apply!!Save changes":FBTN 'bash -c "save_config \"INSTALL_DIR\" %2; \
			save_config \"APT_MATCH\" %3; save_config \"SEARCH_MATCH\" %4; \
			save_config \"AUTO_CLEAN\" %6" ' \
		--button=$'gtk-help':'bash -c "help_dialog \"Install_Config_Help\"" ' \
		--button=gtk-close:1
	fi
}

#### ERROR MESSAGE WINDOW ####
error_dialog(){
	local ERR_REASON="${1}"
	local ERR_SUBJECT="${2}"
	local ERR_TITLE=$"General Error"
	local ERR_LABEL="$ERR_TITLE"
	### Generating message content
	if [[ $ERR_REASON == "Directory" ]]; then
		ERR_TITLE=$"Directory not found"
		ERR_LABEL=$"\n <b>$ERR_SUBJECT</b> is NOT a valid path."
	elif [[ $ERR_REASON == "Sourcefile" ]]; then
		ERR_TITLE=$"No Sourcefile"
		ERR_LABEL=$"\n <b>$ERR_SUBJECT</b> needs to exist for the Offline repo to work."
	elif [[ $ERR_REASON == "Repopath" ]]; then
		ERR_TITLE=$"Not valid Repo"
		ERR_LABEL=$"\n Your folder<b>$ERR_SUBJECT</b> isn't a valid repo folder."
	elif [[ $ERR_REASON == "NoPackage" ]]; then
		ERR_TITLE=$"Package not found"
		ERR_LABEL=$"\n <b>$ERR_SUBJECT</b> is NOT a valid package name."
	elif [[ $ERR_REASON == "PackageFail" ]]; then
		ERR_TITLE=$"Package not processed"
		ERR_LABEL=$"\n Package <b>$ERR_SUBJECT</b> cannot be processed. Contact dev."
	elif [[ $ERR_REASON == "NoSelection" ]]; then
		ERR_TITLE=$"Package not selected"
		ERR_LABEL=$"\n <b>NO package</b> was selected."
	elif [[ $ERR_REASON == "Password" ]]; then
		ERR_TITLE=$"Incorrect Password"
		ERR_LABEL=$"\n <b>SUDO password</b> is wrong."
	elif [[ $ERR_REASON == "DPKG" ]]; then
		ERR_TITLE=$"APT is locked"
		ERR_LABEL=$"\n <b>APT is locked</b> by another program."
	elif [[ $ERR_REASON == "OPTION" ]]; then
		ERR_TITLE=$"Invalid option"
		ERR_LABEL=$"\n <b>$ERR_SUBJECT</b> is not a valid option."
	elif [[ $ERR_REASON == "NO PACKAGELISTS" ]]; then
		ERR_TITLE=$"NO packagelist files"
		ERR_LABEL=$"\n <b>No .packagelist</b> files were found in\n $ERR_SUBJECT"
	elif [[ $ERR_REASON == "Package Excluded" ]]; then
		ERR_TITLE=$"Possible exclusion"
		ERR_LABEL=$"\n Required packages \n<b>$ERR_SUBJECT</b> \n were not added to the dependency list.\n\
They may have possibly been excluded. \nPlease revise or disable the exclude.list file."
	elif [[ $ERR_REASON == "INVALID PACKAGELIST" ]]; then
		ERR_TITLE=$"Non Valid packagelist"
		ERR_LABEL=$"\n <b>$ERR_SUBJECT</b> is not a valid packagelist."
	fi
	### Display error dialog
	echo "Error: $ERR_REASON"
	yad --image="gtk-dialog-error" --borders=10 --center \
		--title="$ERR_TITLE" --class="apt-offline-repo" --name="Error-Window" \
		--form --align=center --separator="" \
		--field="$ERR_LABEL":LBL '' --borders=10 \
		--window-icon=gtk-dialog-error --button=gtk-close:1
}

########################################################################
#################  2. FUNCTIONS THAT DISPLAY WINDOWS  ##################
########################################################################

#### LIST AND DOWNLOAD UPDATES FOR OFFLINE SYSTEM ####
download_updates(){
	### Retrieve user input
	local D_DIRECTORY="${1}"
	local PACKAGE_LIST_NAME="${2}"
	local SELECTED_ARCH="$SYSTEM_ARCH"
	### Path to PACKAGE_LIST
	local PACKAGE_LIST="${SCRIPT_FOLDER}/packagelists/${PACKAGE_LIST_NAME}_${SYSTEM_ARCH}.packagelist"
	
	### verify all dependencies are in place. If not, exit script
	if check_depends true; then echo "Check package list - $PACKAGE_LIST_NAME";
	else echo "one or more necessary utility programs are not currently installed."; return 1; fi
	
	### Check if package_list exists
	if [[ "$PACKAGE_LIST_NAME" != "$THIS_MACHINE" ]] && [ ! -f "$PACKAGE_LIST" ]; then
		error_dialog "INVALID PACKAGELIST" "$PACKAGE_LIST"

	### Check if folder path is broken
	elif [ ! -d "$D_DIRECTORY" ]; then
		error_dialog "Directory" "$D_DIRECTORY"
	
	else
		### Fix Repo directory if needed
		D_DIRECTORY="$(check_repo_folder "$D_DIRECTORY" "$SELECTED_ARCH")"
		cd "${D_DIRECTORY}/${SELECTED_ARCH}"
		### Creating temp files (will delete at the end)
		export TEMP_LIST=$(mktemp)
		export ALL_PACKAGE_LIST=$(mktemp) # all packages and all versions
		export S_PACKAGE_LIST=$(mktemp) # selected package list
		export UPDATE_LIST=$(mktemp)
		export YAD_UPDATE_LIST=$(mktemp)
		export EXTRA_UPDATES=$(mktemp)
		### local variables for faster processing
		local APT_PACKAGE_NVER
		local S_PACKAGE_NVER
		local S_PACKAGE_NAMES
		local S_UPDATE_NVER
		
		### Remove all comments from PACKAGE_LIST and select proper lines
		if [[ "$PACKAGE_LIST_NAME" == "$THIS_MACHINE" ]]; then
			apt --installed list | tail -n+2 | cut -d "[" -f 1 | \
				sed 's/,now / /g' | sed 's/\/now /\/ /g' > $S_PACKAGE_LIST
		else
			cat $PACKAGE_LIST | grep -v "^#" | grep "/" > $S_PACKAGE_LIST
		fi
		S_PACKAGE_NAMES="$(cat $S_PACKAGE_LIST | cut -d "/" -f 1 | sort -u)"
		S_PACKAGE_NVER="$(cat $S_PACKAGE_LIST | sed 's/\/[^ ]* /\/ /g')"
		
		### Remove all temp files
		remove_temp(){
			rm -f -- "$TEMP_LIST"
			rm -f -- "$ALL_PACKAGE_LIST"
			rm -f -- "$S_PACKAGE_LIST"
			rm -f -- "$UPDATE_LIST"
			rm -f -- "$YAD_UPDATE_LIST"
			rm -f -- "$EXTRA_UPDATES"
		}
		### function to list packages
		list_packages(){
			export U_PACKAGE_LIST="${1}"
			### Display list of packages to be downloaded
			yad --title=$"Package list (items slated for downloading)" --width=540 --height=400 \
				--text=$"List of packages ready to update (with sources and architecture)" \
				--borders=10 --center --text-info <"$U_PACKAGE_LIST" \
				--button="URLS":'bash -c url_downlist' \
				--wrap --margins=5 --button=gtk-close:1
		}
		### generate URL download list
		url_downlist(){
			local URL_LIST=$(mktemp)
			local TOTAL_PACKAGES=$( wc -l < $U_PACKAGE_LIST | awk '{ print $1 }')
			local PACKAGE_N=0
			local PACKAGE_URL
			local PACKAGE_NAME
			local PACKAGE_SOURCE
			cd /tmp
			while read -r line; do
				echo $((PACKAGE_N * 100 / TOTAL_PACKAGES))
				PACKAGE_NAME="$(echo $line | cut -d " " -f 1)"
				PACKAGE_SOURCE="$(echo $line | cut -d " " -f 2 | sed 's/ //g')"
				if [[ "$PACKAGE_SOURCE" == "0" ]]; then PACKAGE_SOURCE=""; fi
				if [ -n "$PACKAGE_SOURCE" ]; then PACKAGE_SOURCE="-t $PACKAGE_SOURCE"; fi
				echo $"# Getting URL for ${PACKAGE_NAME%:*} from $PACKAGE_SOURCE"
				PACKAGE_URL="$(apt-get $PACKAGE_SOURCE download $PACKAGE_NAME --print-uris | awk '{print $1}')"
				PACKAGE_URL="$(echo "$PACKAGE_URL" | sed "s/'//g")"
				### Save the URL to URL_LIST if a URL was generated
				if [[ "$PACKAGE_URL" != "" ]]; then
					echo $PACKAGE_URL >> $URL_LIST
				fi
				PACKAGE_N=$((++PACKAGE_N))
			done <"$U_PACKAGE_LIST" | yad --title="." --progress --center --borders=20 \
			--no-buttons --auto-close --height=100 --title=$"URL list"
			
			### Display list of URLs
			yad --title=$"URL list" --width=600 --height=400 --margins=5 \
				--borders=10 --center --text-info <"$URL_LIST" --nowrap \
				--button=gtk-close:1
				
			### remove tempfile
			rm -f -- "$URL_LIST"
		}
		export -f remove_temp
		export -f list_packages
		export -f url_downlist
		
		### Exit function if file is emptyIf 
		if [[ ! -s $S_PACKAGE_LIST ]]; then
			error_dialog "INVALID PACKAGELIST" "$PACKAGE_LIST"
			### Clean temp file
			remove_temp
			return 1
		fi
		
		### Update local packagelist
		#update_apt
		
		### Get list of all packages (and all versions)
		while true; do
			echo $"Fetching a list of all packages available"
			# Strip all other information except for version and architecture of first available apps.
			apt list | tail -n+2 | cut -d "[" -f 1 | sed 's/\/[^ ]* /\/ /g' > $TEMP_LIST
			# Strip /now and unknown sources to get all posible version available in repos
			apt -a list | tail -n+2 | cut -d "[" -f 1 | sed 's/,now / /g' | sed 's/\/now /\/ /g' | grep -v "/ " > $ALL_PACKAGE_LIST
			break
		done | yad --progress --pulsate --center --borders=20 \
			--progress-text=$"Fetching all packages available"  \
			--no-buttons --auto-close --height=100 --title=$"Checking for updates"
		
		### Compare to find possible updates based on architecture and version
		S_UPDATE_NVER="$(diff $TEMP_LIST <(echo "$S_PACKAGE_NVER") | grep "^>" | cut -c3-)"
		
		### No updates found > Exit script
		if [ ! -n "$S_UPDATE_NVER" ]; then
			echo "No updates found"
			yad --center --width=350 --text-align=center \
				--text=$"\nThere are no new updates available right now for\n<b>$PACKAGE_LIST_NAME</b>." \
				--margins=10 --button='gtk-close':1 --title=$"No Updates" --borders=5 \
				--window-icon=gtk-dialog-info --image="gtk-dialog-info"
			### Clean temp files
			remove_temp
			return 1
		fi
		
		### Rebuild ALL PACKAGES list to only contain possible updates
		local ALL_PACKAGE_INFO="$(cat $ALL_PACKAGE_LIST | grep -f <(echo "$S_UPDATE_NVER" | sed 's/\/.*/\//g'))"
		echo "$S_UPDATE_NVER" > $TEMP_LIST
		
		### Starting to record errors
		echo "$(date) - Errors for Updating ${PACKAGE_LIST_NAME}:${SELECTED_ARCH}" >> 0_errors.log
		
		### Preparing variables
		local PACKAGE_NAME
		local PACKAGE_SOURCE
		local PACKAGE_VERSION
		local PACKAGE_ARCH
		local CANDIDATE_INFO
		local CANDIDATE_SOURCE
		local CANDIDATE_VERSION
		# Save file to variable for faster search
		local S_PACKAGE_INFO="$(cat $S_PACKAGE_LIST)"
		
		### Preparing variables for update check
		local UPDATE_N=0
		local TOTAL_UPDATES=$( wc -l < $TEMP_LIST | awk '{ print $1 }')
		### Compare Source and version for each of the possible updates
		echo "Checking for updates"
		while read -r line; do
			echo $((UPDATE_N * 100 / TOTAL_UPDATES))
			echo $"# Checking for updates"
			PACKAGE_NAME="$(echo "$line" | cut -d "/" -f 1)"
			PACKAGE_VERSION="$(echo $line | cut -d" " -f 2)"
			PACKAGE_ARCH="$(echo "$line" | cut -d " " -f 3)"
			PACKAGE_SOURCE="$(echo "$S_PACKAGE_INFO" | grep "^${PACKAGE_NAME}/"| cut -d" " -f 1 | cut -d "/" -f 2)"
			PACKAGE_SOURCE="${PACKAGE_SOURCE%%,*}"
			CANDIDATE_INFO="$(echo "$ALL_PACKAGE_INFO" | grep "^${PACKAGE_NAME}/" | grep " $PACKAGE_ARCH")"
			
			### If package doesn't exists, skip this package
			if [ ! -n "$CANDIDATE_INFO" ]; then
				echo "Skiping ${PACKAGE_NAME}:${PACKAGE_ARCH}. Not in packagelist"
				echo "	${PACKAGE_NAME}:${PACKAGE_ARCH} cannot be found. It cannot be updated." >> 0_errors.log
			else
				### Backports confirmed, only fetch for backports info
				if [ $(echo "$PACKAGE_SOURCE" | grep -ic "backports") -gt 0 ] && \
				[ $(echo "$CANDIDATE_INFO" | grep -ic "$PACKAGE_SOURCE") -gt 0 ]; then
					CANDIDATE_INFO="$(echo "$CANDIDATE_INFO" | grep -m1 "$PACKAGE_SOURCE ")"
				### Normal source (non-backports), save newest version information
				else
					CANDIDATE_INFO="$(echo "$CANDIDATE_INFO" | grep -v "\-backports" | head -n 1)"
				fi
				
				### Check if update is found, compare versions and save update info
				if [ -n "$CANDIDATE_INFO" ]; then
					CANDIDATE_SOURCE="$(echo "$CANDIDATE_INFO" | cut -d" " -f 1 | cut -d "/" -f 2)"
					CANDIDATE_SOURCE="${CANDIDATE_SOURCE%%,*}"
					CANDIDATE_VERSION="$(echo "$CANDIDATE_INFO" | cut -d" " -f 2)"
					#echo "$PACKAGE_NAME could update from $PACKAGE_VERSION to $CANDIDATE_VERSION"
					# if Update is really available, add to list
					if $(dpkg --compare-versions $PACKAGE_VERSION lt $CANDIDATE_VERSION); then
						# Export info to yad list
						echo "$PACKAGE_NAME" >> $YAD_UPDATE_LIST
						echo "$CANDIDATE_SOURCE" >> $YAD_UPDATE_LIST
						echo "$PACKAGE_VERSION" >> $YAD_UPDATE_LIST
						echo "$CANDIDATE_VERSION" >> $YAD_UPDATE_LIST
						# Export for downloading later
						echo "${PACKAGE_NAME}:${PACKAGE_ARCH} $CANDIDATE_SOURCE" >> $UPDATE_LIST
						#echo "${PACKAGE_NAME}:${PACKAGE_ARCH} wants to update from $PACKAGE_VERSION to $CANDIDATE_VERSION"
					fi
				else
					echo "${PACKAGE_NAME}:${PACKAGE_ARCH} cannot be found in package lists for $PACKAGE_SOURCE"
				fi
			fi
			UPDATE_N=$((++UPDATE_N))
		done <"$TEMP_LIST" | yad --progress --center --borders=20 \
			--progress-text=$"Checking for updates" \
			--no-buttons --auto-close --height=100 --title=$"Checking for updates"
		
		### Check if there is no updates and inform the user
		TOTAL_UPDATES=$( wc -l < $UPDATE_LIST | awk '{ print $1 }')
		if [ $TOTAL_UPDATES -eq 0 ]; then
			echo "No updates could be checked"
			### Clean temp files
			remove_temp
			return 1
		fi
		echo "Total Updates found for $PACKAGE_LIST_NAME: $TOTAL_UPDATES"
		TEXT_UPDATES=$"Total updates for $PACKAGE_LIST_NAME: <b>$TOTAL_UPDATES</b>"
		
		### Also list new dependencies for updates (if enabled)
		local UPDATE_EXTRA="$(check_config "LOAD" "UPDATE_EXTRA")"
		if [[ "${UPDATE_EXTRA,,}" == "true" ]]; then
			echo $"Checking for extra-updates"
			
			#~ ### EXTREMELY SLOW method but that gives more info
			#~ cat $UPDATE_LIST > $EXTRA_UPDATES
			#~ UPDATE_N=0
			#~ while read -r line; do
				#~ #echo $"# Checking for extra-updates"
				#~ echo $((UPDATE_N * 100 / TOTAL_UPDATES))
				#~ PACKAGE_NAME="$(echo "$line" | cut -d" " -f 1)"
				#~ PACKAGE_SOURCE="$(echo $line | cut -d" " -f 2)"
				#~ DIRECT_DEPENDENCIES="$(apt-cache -t "$PACKAGE_SOURCE" depends $PACKAGE_NAME \
				#~ --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances \
				#~ --no-pre-depends -o APT::Cache::ShowOnlyFirstOr=true | sed 's/^.*: //g' | sed 's/ //g' | \
				#~ sed -e s/'<'/''/ -e s/'>'/''/ | grep -v ":" | sort -u)"
				#~ NEW_UPDATES="$(diff <(echo "$S_PACKAGE_NAMES") <(echo "$DIRECT_DEPENDENCIES") | grep "^>" | cut -c3-)"
				#~ if [ -n "$NEW_UPDATES" ]; then
					#~ for line2 in $NEW_UPDATES; do
						#~ # Export and an update if not already there
						#~ if [ $(cat $YAD_UPDATE_LIST | grep -xc "$line2") -eq 0 ]; then
							#~ echo "$line2" >> $YAD_UPDATE_LIST
							#~ echo "$PACKAGE_SOURCE" >> $YAD_UPDATE_LIST
							#~ echo " " >> $YAD_UPDATE_LIST
							#~ echo $"new from ${PACKAGE_NAME%:*}" >> $YAD_UPDATE_LIST
							#~ if [ $(echo "$PACKAGE_SOURCE"  | grep -c "backports") -eq 0 ]; then
								#~ echo "${line2} 0" >> $UPDATE_LIST
							#~ else
								#~ echo "${line2} $PACKAGE_SOURCE" >> $UPDATE_LIST
							#~ fi
							
						#~ fi
					#~ done
				#~ fi
				#~ UPDATE_N=$((++UPDATE_N))
			#~ done <"$EXTRA_UPDATES" | yad --progress --center --borders=20 \
			#~ --progress-text=$"Checking for extra-updates"  \
			#~ --no-buttons --auto-close --height=100 --title=$"Checking for extra-updates"
			
			### FAST METHOD
			while true; do
				echo $"Checking for extra-updates"
				BACKPORTS_LIST="$(cat $UPDATE_LIST | grep "\-backports")"
				NON_BACKPORTS_LIST="$(cat $UPDATE_LIST | grep -v "\-backports")"
				### Backports check
				if [ -n "$BACKPORTS_LIST" ]; then
					BACKPORTS_SOURCE="$(echo "$BACKPORTS_LIST" | head -n 1 | cut -d" " -f 2)"
					BACKPORTS_PACKAGES="$(echo "$BACKPORTS_LIST" | cut -d" " -f 1)"
					DIRECT_DEPENDENCIES="$(apt-cache -t "$BACKPORTS_SOURCE" depends $BACKPORTS_LIST \
					--no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances \
					--no-pre-depends -o APT::Cache::ShowOnlyFirstOr=true | sed 's/^.*: //g' | grep "^\w" | \
					sed -e s/'<'/''/ -e s/'>'/''/ | grep -v ":" | sort -u)"
					# Only save if they are not already
					diff <(echo "$S_PACKAGE_NAMES") <(echo "$DIRECT_DEPENDENCIES") | \
					grep "^>" | cut -c3- | sed -e "s/$/ $BACKPORTS_SOURCE/" >> $EXTRA_UPDATES
				fi
				### Non-backports check
				if [ -n "$NON_BACKPORTS_LIST" ]; then
					NON_BACKPORTS_PACKAGES="$(echo "$NON_BACKPORTS_LIST" | cut -d" " -f 1)"
					DIRECT_DEPENDENCIES="$(apt-cache depends $NON_BACKPORTS_PACKAGES \
					--no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances \
					--no-pre-depends -o APT::Cache::ShowOnlyFirstOr=true | sed 's/^.*: //g' | grep "^\w" | \
					sed -e s/'<'/''/ -e s/'>'/''/ | grep -v ":" | sort -u)"
					# Only save if they are not already
					diff <(echo "$S_PACKAGE_NAMES") <(echo "$DIRECT_DEPENDENCIES") | \
					grep "^>" | cut -c3- | sed -e 's/$/ 0/' >> $EXTRA_UPDATES
				fi
				### Export new packages to update list
				if [ -s "$EXTRA_UPDATES" ]; then
					# Retrieving package info for each new package
					CANDIDATE_INFO="$(cat $EXTRA_UPDATES | cut -d" " -f1 | sed -e 's/$/\//')"
					ALL_PACKAGE_INFO="$(cat $ALL_PACKAGE_LIST | grep -f <(echo "$CANDIDATE_INFO"))"
					while read -r line2; do
						PACKAGE_NAME="$(echo "$line2" | cut -d " " -f 1 | sed 's/ //g')"
						CANDIDATE_INFO="$(echo "$ALL_PACKAGE_INFO" | grep "^${PACKAGE_NAME}/")"
						CANDIDATE_SOURCE="$(echo $line2 | cut -d " " -f 2)"
						# Get correct source and Version, (for backport or anything but backports)
						if [ $(echo "$CANDIDATE_SOURCE" | grep -ic "backports") -gt 0 ] && \
						[ $(echo "$CANDIDATE_INFO" | grep -ic "$CANDIDATE_SOURCE") -gt 0 ]; then
							CANDIDATE_INFO="$(echo "$CANDIDATE_INFO" | grep -m1 "$CANDIDATE_SOURCE ")"
						else
							CANDIDATE_INFO="$(echo "$CANDIDATE_INFO" | grep -v "\-backports" | head -n 1)"
						fi
							CANDIDATE_SOURCE="$(echo "$CANDIDATE_INFO" | cut -d" " -f 1 | cut -d "/" -f 2)"
							CANDIDATE_SOURCE="${CANDIDATE_SOURCE%%,*}"
							CANDIDATE_VERSION="$(echo "$CANDIDATE_INFO" | cut -d" " -f 2)"
						# Export and an update if not already there
						if [ -n "$CANDIDATE_SOURCE" ] && [ $(cat $YAD_UPDATE_LIST | grep -xc "$PACKAGE_NAME") -eq 0 ]; then
							REVERSE_DEPENDS="$(apt-cache rdepends $PACKAGE_NAME --implicit | \
								sed 's/ //g' | sed 's/|//g')"
							REVERSE_DEPENDS="$(echo "$REVERSE_DEPENDS" | \
								grep -f <(cat $UPDATE_LIST | cut -d" " -f1 | cut -d":" -f1) | head -n1)"
							echo "$PACKAGE_NAME" >> $YAD_UPDATE_LIST
							echo "$CANDIDATE_SOURCE" >> $YAD_UPDATE_LIST
							echo $"new from $REVERSE_DEPENDS" >> $YAD_UPDATE_LIST
							echo "$CANDIDATE_VERSION" >> $YAD_UPDATE_LIST
							echo "$PACKAGE_NAME $CANDIDATE_SOURCE" >> $UPDATE_LIST
						fi
					done <"$EXTRA_UPDATES"
				fi
				break
			done | yad --progress --pulsate --center --borders=20 \
			--progress-text=$"Checking for extra-updates"  \
			--no-buttons --auto-close --height=100 --title=$"Checking for extra-updates"
			
			### If new packages were added, modify TEXT_UPDATES
			NEW_UPDATES=$(wc -l < $UPDATE_LIST | awk '{ print $1 }')
			NEW_UPDATES=$( expr $NEW_UPDATES - $TOTAL_UPDATES )
			echo "New extra updates: $NEW_UPDATES"
			if [ $NEW_UPDATES -gt 0 ]; then
				TEXT_UPDATES=$"${TEXT_UPDATES} \nNew packages for $PACKAGE_LIST_NAME: <b>$NEW_UPDATES</b>"
			fi
		fi
		
		### Display available updates
		yad --class="apt-offline-repo" --name="List-Updates" \
		--window-icon=synaptic --title=$"List of Updates to download" --borders=20 \
		--text=$"$TEXT_UPDATES \nList of Updates ready to be downloaded" \
		--text-align=center --center --separator="|" --list  --search-column=4 --column=$"Package" \
		--column=$"Source" --column=$"Original" --column=$"Update" < $YAD_UPDATE_LIST --width=700 --height=400 \
		--button=$"LIST":'bash -c "list_packages \"$UPDATE_LIST\""' \
		--button=$"Download Updates!gtk-save!Download all updates listed":0 --button=gtk-quit:1
		local exitcode=$?
		if [ $exitcode -ne 0 ]; then
			echo "NOT Downloading"
			### Clean temp files
			remove_temp
			return 1
		fi
		
		### Return to repo folder just in case
		cd "${D_DIRECTORY}/${SELECTED_ARCH}"

		### Check if root/has Super User privileges
		check_sudo "true"
		if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
			### Check if DPKG is locked
			if check_sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; then
				### Clean temp file
				remove_temp
				error_dialog "DPKG" "LOCKED"
				return 1
			fi

			### DOWNLOADING UPDATES ###
			echo "Downloading Updates for $PACKAGE_LIST_NAME"
			local UPDATE_N=0
			TOTAL_UPDATES=$( wc -l < $UPDATE_LIST | awk '{ print $1 }')
			local REMOVE_OLD
			local F_PACKAGE_NAME
			local PACKAGE_DEBFILE_NAME
			rm $S_PACKAGE_LIST
			cat "$UPDATE_LIST" > $S_PACKAGE_LIST
			REMOVE_OLD="$(check_config "LOAD" "REMOVE_OLD")"
			while read line; do
				echo $((UPDATE_N * 100 / TOTAL_UPDATES))
				PACKAGE_NAME="$(echo $line | cut -d " " -f 1)"
				PACKAGE_SOURCE="$(echo $line | cut -d " " -f 2 | sed 's/ //g')"
				if [[ "$PACKAGE_SOURCE" == "0" ]]; then PACKAGE_SOURCE=""; fi
				if [ -n "$PACKAGE_SOURCE" ]; then PACKAGE_SOURCE="-t $PACKAGE_SOURCE"; fi
				PACKAGE_DEBFILE_NAME="$(apt-get $PACKAGE_SOURCE download $PACKAGE_NAME --print-uris | awk '{print $2}')"
				echo $"# Downloading ${PACKAGE_NAME%:*}"
				check_sudo apt-get $PACKAGE_SOURCE download $PACKAGE_NAME
				### Check for errors
				F_PACKAGE_NAME="$(apt-get $PACKAGE_SOURCE download $PACKAGE_NAME --print-uris | awk '{print $2}')"
				### Package downloaded correctly; remove from error download list
				if [[ "$F_PACKAGE_NAME" == "" ]]; then
					sed -i -e "/^${line}$/d" "$S_PACKAGE_LIST"
				fi
				### Remove old version
				if [[ "$REMOVE_OLD" == "true" ]] && [[ "$PACKAGE_FILE_NAME" != "" ]]; then
					if [ $(ls -1q | grep -c "^${line}_") -gt 1 ]; then
						for file in ${line%:*}_*; do
							if [[ "$file" != "$PACKAGE_DEBFILE_NAME" ]]; then rm $file; fi
						done
					fi
				fi
				UPDATE_N=$((++UPDATE_N))
			done <"$UPDATE_LIST" | yad --progress  --center --borders=20 --no-buttons \
			--progress-text=$"Downloading..." --auto-close --height=100 --title=$"Downloading..."
			echo "Finished downloading"
			
			### Create packagelist
			while true; do 
				echo $"# Updating the local package list";
				dpkg-scanpackages --multiversion ./ /dev/null | gzip -9c > ./Packages.gz 2>>0_errors.log
			break
			done | yad --progress --pulsate --center --borders=20 \
			--progress-text="Updating the local package list"  \
			--no-buttons --auto-close --height=100 --title=$"Checking packages"
			
			### Check whether all packages were downloaded
			if [ ! -s $S_PACKAGE_LIST ]; then
				echo "Downloaded all updates properly"
				### Inform the user of the positive result
				yad --center --width=350 --text-align=center \
				--text=$"All <b>$TOTAL_UPDATES</b> updates for <b>$PACKAGE_LIST_NAME</b> were downloaded.\n\
\n Your local Repo packagelist has been updated." --margins=5 \
				--button='gtk-close':1 --title=$"Packagelist created" --borders=5 \
				--window-icon=gtk-dialog-info --image="gtk-dialog-info"
			### If there are missing packages or they didn't download properly
			else
				### Save errors in log
				while read line; do
					PACKAGE_NAME="$(echo $line | cut -d " " -f 1)"
					echo "	Error: Update for $PACKAGE_NAME did not download properly (or was skipped)." >> 0_errors.log
				done <"$S_PACKAGE_LIST"
				### Inform the user of the negative result
				yad --center --width=480 --text-align=center \
				--text=$"Some updates for <b>$PACKAGE_LIST_NAME</b>\n could not be saved.\n\n\
Possible reasons: The download couldn't start, or was stopped abruptly.\n\
Click the <b>List</b> button to review the list of failed//skipped updates.\n\n\
Your local Repo packagelist has been updated." \
				--button=$"LIST":'bash -c "list_packages \"$S_PACKAGE_LIST\""' --margins=5 \
				--button='gtk-close':1 --title=$"Error Downloading" --borders=5 \
				--window-icon=gtk-dialog-error --image="gtk-dialog-error"
			fi
		fi
		### remove temp files
		remove_temp
	fi
}

#### LIST DEPENDENCIES AND DOWNLOAD THEM ####
download_dependencies(){
	### Retrieve user input
	local D_DIRECTORY="${1}"
	local PACKAGE_NAME="${2}"
	local SELECTED_ARCH="$SYSTEM_ARCH"
	local PACKAGE_SOURCE="APT"
	local PACKAGE_INFO
	local PACKAGE_DESC
	local FILE_PKG_LIST
	
	### verify all dependencies are in place. If not, exit download script
	if check_depends true; then echo "Check package name - $PACKAGE_NAME";
	else echo "one or more necessary utility programs are not currently installed."; return 1; fi
		
	### Check if package_name is empty
	if [[ "$(echo "$PACKAGE_NAME" | sed 's/ //g')" == "" ]]; then
		error_dialog "NoPackage" "$PACKAGE_NAME"

	### Check if folder path is broken
	elif [ ! -d "$D_DIRECTORY" ]; then
		error_dialog "Directory" "$D_DIRECTORY"
	
	else
		### Select a file instead of searching for packages
		if [[ "$PACKAGE_NAME" == "select_file" ]]; then
			### File selection dialog
			PACKAGE_INFO=$(yad --window-icon=synaptic \
			--title=$"Download from file" --class="apt-offline-repo" --name="File-selection" \
			--borders=10 --center --text-align="center" --width=460 \
			--text=$"Select a file containing a list of packages\n and Download them (+ dependencies)" \
			--form --field=$"Package List file":FL \
			--field=$"List Name":CE "" --field=$"Description":CE "" \
			--button=$"Download!gtk-goto-bottom!Download packages and dependencies from list in file":0 \
			--button='gtk-cancel':1)
			local exitcode=$?
			if [ $exitcode -eq 0 ]; then
				## Process file
				PACKAGE_SOURCE="FILE"
				FILE_PKG_LIST="$(echo "$PACKAGE_INFO" | cut -d "|" -f 1)"
				PACKAGE_NAME="$(echo "$PACKAGE_INFO" | cut -d "|" -f 2)"
				PACKAGE_DESC="$(echo "$PACKAGE_INFO" | cut -d "|" -f 3)"
				
				### Check that the file selected is a text file
				if [ $(file "$FILE_PKG_LIST" | cut -d":" -f2- | grep -c "text") -gt 0 ] && \
				[ -f "$FILE_PKG_LIST" ]; then
					## If no name provided, use filename
					if [[ "$(echo "$PACKAGE_NAME" | sed 's/ //g')" == "" ]]; then
						PACKAGE_NAME="${FILE_PKG_LIST##*/}"
					fi
				else
					error_dialog "INVALID PACKAGELIST" "$FILE_PKG_LIST"
					return 1
				fi
			else
				return 1
			fi
			
		### Search for packages from PI and APT cache
		else
			PACKAGE_INFO="$(search_package "$PACKAGE_NAME")"
			if [[ "$PACKAGE_INFO" == "" ]]; then
				error_dialog "NoPackage" "$PACKAGE_NAME";
				return 1
			elif [[ "$PACKAGE_INFO" == "+nothing+" ]]; then
				error_dialog "NoSelection" "$PACKAGE_NAME";
				return 1
			fi
			
			### Replace new information
			PACKAGE_SOURCE="$(echo "$PACKAGE_INFO" | cut -d "|" -f 1)"
			PACKAGE_NAME="$(echo "$PACKAGE_INFO" | cut -d "|" -f 2)"
			PACKAGE_DESC="$(echo "$PACKAGE_INFO" | cut -d "|" -f 3)"
		fi
		### Fix Repo directory if needed
		D_DIRECTORY="$(check_repo_folder "$D_DIRECTORY" "$SELECTED_ARCH")"
		
		### Move to download folder
		cd "${D_DIRECTORY}/${SELECTED_ARCH}"
		### Error handling
		echo "$(date) - Errors for package ${PACKAGE_NAME}:${SELECTED_ARCH}" >> 0_errors.log
		
		### informing of selection
		echo "Package selection: $PACKAGE_NAME, Source: $PACKAGE_SOURCE, \
Package Arch: $SELECTED_ARCH, Directory: $D_DIRECTORY, OS-Architecture: $SYSTEM_ARCH"
		
		### Creating package list for future install and SPECIAL_SOURCE in case of special source
		local PACKAGE_LIST=""
		local SPECIAL_SOURCE=""
		local NEW_SOURCE=""
		### Create temp file for processing info
		TEMP_FILE=$(mktemp) #create temp file for package data
		TEMP_FILE2=$(mktemp) #create second temp file for processing data
		DOWNLOAD_LIST=$(mktemp) #create a temp file for listing all downloaded packages
		### Export temp files
		export TEMP_FILE=$TEMP_FILE
		export TEMP_FILE2=$TEMP_FILE2
		export DOWNLOAD_LIST=$DOWNLOAD_LIST
		
		### function to clean all dependency lists and repo list changes
		clean_pi_repo(){
			PRE_INST="${1}"
			INST="${2}"
			POST_INST="${3}"
			CLEAN_SOURCES="${4}"
			### Clean sourcelists
			if [[ "$CLEAN_SOURCES" != "" ]]; then
				echo "Removing changes from sourcelists"
				### Cleaning new sourcelists using post-intructions (for debian.list)
				if [ $(echo "$CLEAN_SOURCES" | grep -c "debian.list") -gt 0 ]; then
					echo "repairing debian.list"
					check_sudo bash "$POST_INST"
				else
					for word in $CLEAN_SOURCES; do
						if [ $(echo "$word" | grep -c "various.list") -eq 0 ]; then
							### remove sourcelist
							echo "removing $word"
							check_sudo rm -f "$word"
						fi
					done
				fi
				### update packagelist
				echo "updating packagelist"
				while true; do
					echo $"# Updating package list"
					check_sudo apt update
					break
				done | yad --progress --pulsate --center --borders=20 \
					--progress-text=$"Updating packagelist"  \
					--no-buttons --auto-close --height=100 --title=$"Cleanup"
			fi
			### remove temp files
			rm -f -- "$PRE_INST"
			rm -f -- "$INST"
			rm -f -- "$POST_INST"
		}
		### List all dependencies that will be downloaded
		list_packages(){
			export U_PACKAGE_LIST="${1}"
			### Display list of packages to be downloaded
			yad --title=$"Package list (items slated for downloading)" --width=540 --height=400 \
				--text=$"Depending on your my-offline-repo configuration choices, \
the list of packages to be downloaded may not include the ones listed in the exclude.list file (or its dependencies).\n\n\
If you want to see and download all packages without filtering, UNTICK the option 'Use exclude list' in Configuration." \
				--borders=10 --center --text-info <"$U_PACKAGE_LIST" \
				--button="URLS":'bash -c url_downlist' \
				--wrap --margins=5 --button=gtk-close:1
		}
		### generate URL download list
		url_downlist(){
			local URL_LIST=$(mktemp)
			local TOTAL_PACKAGES=$( wc -l < $U_PACKAGE_LIST | awk '{ print $1 }')
			local PACKAGE_N=0
			local PACKAGE_URL
			cd /tmp
			while read -r line; do
				echo $((PACKAGE_N * 100 / TOTAL_PACKAGES))
				echo $"# Getting URL for $line"
				PACKAGE_URL="$(apt-get $SPECIAL_SOURCE download $line --print-uris | awk '{print $1}')"
				PACKAGE_URL="$(echo "$PACKAGE_URL" | sed "s/'//g")"
				### Save the URL to URL_LIST if a URL was generated
				if [[ "$PACKAGE_URL" != "" ]]; then
					echo $PACKAGE_URL >> $URL_LIST
				fi
				PACKAGE_N=$((++PACKAGE_N))
			done <"$U_PACKAGE_LIST" | yad --title="." --progress --center --borders=20 \
			--no-buttons --auto-close --height=100 --title=$"URL list"
			
			### Display list of URLs
			yad --title=$"URL list" --width=600 --height=400 --margins=5 \
				--borders=10 --center --text-info <"$URL_LIST" --nowrap \
				--button=gtk-close:1
				
			### remove tempfile
			rm -f -- "$URL_LIST"
		}
		
		### Remove all temp files
		remove_temp(){
			rm -f -- "$TEMP_FILE"
			rm -f -- "$TEMP_FILE2"
			rm -f -- "$DOWNLOAD_LIST"
		}
		
		### CREATING DEPENDENCY LIST ###
		### FILE with a list of packages, ready for download ##
		if [[ "$PACKAGE_SOURCE" == "FILE" ]]; then
			### Retrieve all package names from FILE and make it a one-liner
			PACKAGE_LIST="$(cat "$FILE_PKG_LIST" | grep -v "^#" | sed 's/,/ /g')"
			PACKAGE_LIST="$(echo $PACKAGE_LIST | tr -s ' ')"
			
			### Build dependency list
			for word in $PACKAGE_LIST; do
				echo $"# Building dependency list for $word"
				### Build dependency list
				build_dependencies "$word" "$TEMP_FILE2" "$SPECIAL_SOURCE"
			done | yad --progress --pulsate --center --borders=20 \
				--progress-text=$"Getting Dependencies" \
				--no-buttons --auto-close --height=100 --title=$"Getting Dependencies"
			
			### clean duplicates
			cat "$TEMP_FILE2" | sort -u > $TEMP_FILE
			echo "Temp file-> $TEMP_FILE"
			
		### PI download ##
		elif [[ "$PACKAGE_SOURCE" == "PI" ]]; then
			### Variable to determine which step is being processed
			local INST_STEP=0
			### Temp files containing instructions
			PRE_INSTRUCTIONS=$(mktemp)
			INSTRUCTIONS=$(mktemp)
			POST_INSTRUCTIONS=$(mktemp)
			echo "Retrieving package info from /usr/share/packageinstaller-pkglist/${PACKAGE_NAME}.pm"
			while read -r line; do
				### Don't save any line
				if [[ "$line" == "</preinstall>" ]] || [[ "$line" == "</install_package_names>" ]] || [[ "$line" == "</postinstall>" ]];
				then INST_STEP=0; fi
				### If correct step, save line
				if [ $INST_STEP -eq 1 ] && [[ "$line" != "" ]]; then echo "$line" >> $PRE_INSTRUCTIONS; fi
				if [ $INST_STEP -eq 2 ] && [[ "$line" != "" ]]; then echo "$line" >> $INSTRUCTIONS; fi
				if [ $INST_STEP -eq 3 ] && [[ "$line" != "" ]]; then echo "$line" >> $POST_INSTRUCTIONS; fi
				### Next line, save content
				if [[ "$line" == "<preinstall>" ]]; then INST_STEP=1; fi
				if [[ "$line" == "<install_package_names>" ]]; then INST_STEP=2; fi
				if [[ "$line" == "<postinstall>" ]]; then INST_STEP=3; fi
			done < <(cat "/usr/share/packageinstaller-pkglist/${PACKAGE_NAME}.pm")
			
			### PI CASE 1: EXTERNAL DOWNLOAD OF .deb PACKAGES #
			if [ ! -s "$INSTRUCTIONS" ] && [ -s "$PRE_INSTRUCTIONS" ]; then
				local DOWNLOAD_COM=""
				local PACKAGE_FILE=""
				local DEPENDENCY_LIST=""
				### check if using wget (must be a .deb file)
				if [ $(cat "$PRE_INSTRUCTIONS" | grep "^wget" | grep -c ".deb") -gt 0 ]; then
					### Download command
					DOWNLOAD_COM="$(cat "$PRE_INSTRUCTIONS" | grep ".deb" | grep -m 1 "^wget")"
					### Deb package file to be downloaded
					PACKAGE_FILE="${DOWNLOAD_COM##*/}"
				### check if using curl (must be a .deb file)
				elif [ $(cat "$PRE_INSTRUCTIONS" | grep "^curl" | grep -c ".deb") -gt 0 ]; then
					### Download command
					DOWNLOAD_COM="$(cat "$PRE_INSTRUCTIONS" | grep ".deb" | grep -m 1 "^curl" | grep -oP 'http.?://\S+')"
					### Deb package file to be downloaded
					PACKAGE_FILE="${DOWNLOAD_COM##*/}"
					### Convert download command to wget command
					DOWNLOAD_COM="wget ${DOWNLOAD_COM}"
				fi
				### Process dependencies if package and download command exist, and download
				if [[ "$DOWNLOAD_COM" != "" ]] && [[ "$PACKAGE_FILE" != "" ]]; then
					### check if package already there. If so, delete old package (only for wget)
					if [ -e "$PACKAGE_FILE" ]; then rm "$PACKAGE_FILE"; fi
					echo $"# Downloading $PACKAGE_FILE"
					### download .deb package
					eval $DOWNLOAD_COM --progress=dot 2>&1 | awk '{print $7}; system("")' | \
						sed -u 's/%//' | yad --title $"Downloading..." --progress --height=100 \
						--center --text-align=center --text=$"Downloading $PACKAGE_FILE" \
						--percentage=0 --borders=20 --no-buttons --auto-close
					
					### Get package name and dependencies
					PACKAGE_LIST="$(dpkg-deb -I "$PACKAGE_FILE" | grep "Package: " | cut -d " " -f 3-)"
					DEPENDENCY_LIST="$(dpkg-deb -I "$PACKAGE_FILE" | grep "Depends:" | cut -d " " -f 3- | sed 's/([^()]*)//g' | sed 's/,//g' | sed 's/  / /g')"
					
					### Creating full dependency list
					for word in $DEPENDENCY_LIST; do
						echo $"# Building dependency list for $word"
						echo "$word" >> $TEMP_FILE2
						build_dependencies "$word" "$TEMP_FILE2"
					done | yad --progress --pulsate --center --borders=20 \
						--progress-text=$"Getting Dependencies"  \
						--no-buttons --auto-close --height=100 --title=$"Getting Dependencies"
					### clean duplicates
					cat "$TEMP_FILE2" | sort -u > $TEMP_FILE
				### cannot process file right now
				else
					### Clean packages and sourcelist
					if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
						clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
					fi
					### Clean temp file
					remove_temp
					echo "Error: No wget or curl command found"
					error_dialog "PackageFail" "$PACKAGE_NAME";
					return 1
				fi				
				
			### PI CASE 2: INSTRUCTIONS INCLUDED, USING APT TO DOWNLOAD #
			elif [ -s "$INSTRUCTIONS" ]; then
				### PI CASE 2.A - THERE ARE NO PREINSTRUCTIONS
				if [[ ! -s "$PRE_INSTRUCTIONS" ]]; then
					echo "No Preinstructions. Building packagelist"
				### PI CASE 2.B - THERE ARE PREINSTRUCTIONS, AND CREATING A NEW SOURCELIST FILE IS NECESSARY
				elif [ $(cat "$PRE_INSTRUCTIONS" | grep -c "sources.list.d") -gt 0 ]; then
					echo "Pre Instructions require creating a new file in sources.list.d" 
					### Build list of new source.list files created
					local SOURCELIST=""
					while read -r line; do
						SOURCELIST="${line##*sources.list.d}"
						SOURCELIST="${SOURCELIST%%.list*}"
						SOURCELIST="/etc/apt/sources.list.d${SOURCELIST}.list"
						### save sourcelist path
						if [[ "$NEW_SOURCE" == "" ]] && [[ "$SOURCELIST" != "" ]]; then
							NEW_SOURCE="$SOURCELIST"
						elif [[ "$NEW_SOURCE" != "" ]] && [[ "$SOURCELIST" != "" ]]; then
							NEW_SOURCE="${NEW_SOURCE} ${SOURCELIST}"
						fi
					done < <(cat "$PRE_INSTRUCTIONS" | grep "sources.list.d")
					### Run PRE_INSTRUCTIONS
					## Check if root/has Super User privileges
					check_sudo "true"
					if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
						while true; do
							echo $"# Adding sources list and updating package list"
							check_sudo bash "$PRE_INSTRUCTIONS"
							break
						done | yad --progress --pulsate --center --borders=20 \
							--progress-text=$"Updating packagelist"  \
							--no-buttons --auto-close --height=100 --title=$"Cleanup"
					else
						### Clean packages and sourcelist
						if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
							clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
						fi
						### Clean temp file
						remove_temp
						### inform of error
						echo "Error: Unknown program (no idea what to do)"
						error_dialog "PackageFail" "$PACKAGE_NAME"
						return 1
					fi
				### PI CASE 2.C - CASE NOT YET CONTEMPLATED
				else
					### Clean packages and sourcelist
					if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
						clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
					fi
					### Clean temp file
					remove_temp
					### inform of error
					echo "Error: Unknown program (no idea what to do)"
					error_dialog "PackageFail" "$PACKAGE_NAME"
					return 1
				fi
				
				### Check if a Special source is used and save it in SPECIAL_SOURCE as a string
				local PACKAGE_LINE=""
				if [ $(cat "$INSTRUCTIONS" | grep -Ec "^-t ") -gt 0 ];then
					SPECIAL_SOURCE="$(cat "$INSTRUCTIONS" | grep -E -m 1 "^-t ")"
					PACKAGE_LINE="${SPECIAL_SOURCE##* }"
					SPECIAL_SOURCE="${SPECIAL_SOURCE//$PACKAGE_LINE/}"
					echo "Source: $SPECIAL_SOURCE"
				fi
				
				### Build package list
				while read -r line; do
					PACKAGE_LINE="$line"
					if [ $(echo "$line" | grep -Ec "^-t ") -gt 0 ];then
						PACKAGE_LINE="${line##* }"
						#echo "$line contains $PACKAGE_LINE"
					fi
					### Add package name for installation
					PACKAGE_LIST="${PACKAGE_LIST}${PACKAGE_LINE} "
				done < "$INSTRUCTIONS"
				
				### Build dependency list
				for word in $PACKAGE_LIST; do
					echo $"# Building dependency list for $PACKAGE_LINE"
					### Build dependency list
					build_dependencies "$word" "$TEMP_FILE2" "$SPECIAL_SOURCE"
				done | yad --progress --pulsate --center --borders=20 \
					--progress-text=$"Getting Dependencies" \
					--no-buttons --auto-close --height=100 --title=$"Getting Dependencies"
				
				### clean duplicates
				cat "$TEMP_FILE2" | sort -u > $TEMP_FILE
				
			### PI CASE 3: NO IDEA WHAT TO DO
			else
				### Clean packages and sourcelist
				if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
					clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
				fi
				### Clean temp file
				remove_temp
				### inform of error
				echo "Error: Unknown program (no idea what to do)"
				error_dialog "PackageFail" "$PACKAGE_NAME"
				return 1
			fi
			
		## For APT download option
		elif [[ "$PACKAGE_SOURCE" == "APT" ]]; then
			### Build Dependecy list
			build_dependencies "$PACKAGE_NAME" "$TEMP_FILE"
			
			### packagelist for output repo file
			PACKAGE_LIST="${PACKAGE_NAME}"
		fi
		
		### Delete duplicates in multiarch support
		for package in $(cat "$TEMP_FILE" | grep ":"); do
			if [ "$(cat "$TEMP_FILE" | grep -xc "${package%:*}")" -gt 0 ]; then
				### remove duplicates depending if same arch or not
				if [[ "$SYSTEM_ARCH" == "$SELECTED_ARCH" ]]; then
					sed -i -e "/^${package}$/d" "$TEMP_FILE"
				else
					sed -i -e "/^${package%:*}$/d" "$TEMP_FILE"
				fi
			fi
		done
		
		### Remove specific packages from the dependency list using the EXCLUDE_LIST
		USE_EXCLUDE="$(check_config "LOAD" "EXCLUDE_LIST")"
		if [[ "${USE_EXCLUDE,,}" == "true" ]]; then
			### Check if Exclude file exists and build Exlude package list
			local EXCLUDE_FILE="${SCRIPT_FOLDER}/exclude.list"
			if [ -f "$EXCLUDE_FILE" ]; then
				EXCLUDE_PACKAGES="$(cat $EXCLUDE_FILE | grep -v "^#")"
				EXCLUDE_LIST=$(mktemp)
				FINAL_D_LIST=$(mktemp)
				#~ apt-cache depends $EXCLUDE_PACKAGES --recurse --no-recommends \
				#~ --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances | \
				#~ grep "^\w" | sort -u > $EXCLUDE_LIST
				# Exclude all packages from the current package list
				echo "$EXCLUDE_PACKAGES" > $EXCLUDE_LIST
				diff "$EXCLUDE_LIST" "$TEMP_FILE" | grep "^>" | cut -c3- > $FINAL_D_LIST
				if [[ -s "$FINAL_D_LIST" ]]; then
					echo "Excluding packages in exclude.list"
					cat $FINAL_D_LIST > $TEMP_FILE
				fi
				# Remove temp files
				rm -f -- "$EXCLUDE_LIST"
				rm -f -- "$FINAL_D_LIST"
			else
				# Create file in case it doesn't exist
				touch "$EXCLUDE_FILE"
			fi
		fi
		
		### Total packages:
		local TOTAL_PACKAGES="$(wc -l "$TEMP_FILE" | awk '{ print $1 }')"
		### No dependencies means no packages can be downloaded
		if [ $TOTAL_PACKAGES -eq 0 ]; then
			echo "Error: Dependency list is empty"
			### Clean packages and sourcelist
			if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
				clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
			fi
			### Clean temp file
			remove_temp
			error_dialog "PackageFail" "$PACKAGE_NAME";
			return 1
		fi
		
		### Check if main packages have been excluded
		local EXCLUDED_NAMES
		for word in $PACKAGE_LIST; do
			if [ $(cat $TEMP_FILE | grep -icx "$word") -eq 0 ]; then
				EXCLUDED_NAMES="${EXCLUDED_NAMES} $word"
				echo "	WARNING: Required package $word not in the dependency list. Possible exclusion." \
				| tee -a 0_errors.log
			fi
		done
		if [[ "$(echo "$EXCLUDED_NAMES" | sed 's/ //g')" != "" ]]; then
			error_dialog "Package Excluded" "$EXCLUDED_NAMES";
		fi
		
		### Export function and variable for LIST window
		export -f list_packages
		export -f url_downlist
		export TEMP_FILE=$TEMP_FILE
		export SPECIAL_SOURCE="$SPECIAL_SOURCE"
		
		### Download confirmation
		yad --image="gtk-dialog-info" --borders=10 --center \
		--title=$"Downloading $PACKAGE_NAME" --class="apt-offline-repo" \
		--name="download-check" --form --align=center --separator="" \
		--field=$"\nPackage selection: <b>$PACKAGE_NAME</b>":LBL '' \
		--field=$"Repo folder: <b>${D_DIRECTORY}</b> (${SELECTED_ARCH})":LBL '' \
		--field=$"Total packages: <b>$TOTAL_PACKAGES</b>":LBL '' \
		--field=$"Do you want to download them?":LBL '' \
		--button=$"LIST":'bash -c "list_packages \"$TEMP_FILE\""' --button=gtk-yes:0 --button=gtk-cancel:1
		local exitcode=$?
		if [ $exitcode -ne 0 ]; then
			echo "NOT Downloading"
			### Clean packages and sourcelist
			if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
				clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
			fi
			### Clean temp file
			remove_temp
			return 1
		fi

		### Return to repo folder just in case
		cd "${D_DIRECTORY}/${SELECTED_ARCH}"

		### Check if root/has Super User privileges
		check_sudo "true"
		if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
			### Check if DPKG is locked
			if check_sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; then
				### Clean packages and sourcelist
				if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
					clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
				fi
				### Clean temp file
				remove_temp
				error_dialog "DPKG" "LOCKED"
				return 1
			fi

			### DOWNLOADING PACKAGES ###
			echo "Starting the downloads for $PACKAGE_NAME"
			local TOTAL_PACKAGES=$( wc -l < $TEMP_FILE | awk '{ print $1 }')
			local PACKAGE_N=0
			local REMOVE_OLD
			local F_PACKAGE_NAME
			local PACKAGE_DEBFILE_NAME
			cat "$TEMP_FILE" > $DOWNLOAD_LIST
			REMOVE_OLD="$(check_config "LOAD" "REMOVE_OLD")"
			while read line; do
				echo $((PACKAGE_N * 100 / TOTAL_PACKAGES))
				echo $"# Downloading $line"
				PACKAGE_DEBFILE_NAME="$(apt-get $SPECIAL_SOURCE download $line --print-uris | awk '{print $2}')"
				check_sudo apt-get $SPECIAL_SOURCE download $line
				### Check for errors
				F_PACKAGE_NAME="$(apt-get $SPECIAL_SOURCE download $line --print-uris | awk '{print $2}')"
				### Package downloaded correctly; remove from error download list
				if [[ "$F_PACKAGE_NAME" == "" ]]; then
					sed -i -e "/^${line}$/d" "$DOWNLOAD_LIST"
				fi
				### Remove old version
				if [[ "$REMOVE_OLD" == "true" ]] && [[ "$PACKAGE_FILE_NAME" != "" ]]; then
					if [ $(ls -1q | grep -c "^${line}_") -gt 1 ]; then
						for file in ${line%:*}_*; do
							if [[ "$file" != "$PACKAGE_DEBFILE_NAME" ]]; then rm $file; fi
						done
					fi
				fi
				PACKAGE_N=$((++PACKAGE_N))
			done <"$TEMP_FILE" | yad --progress  --center --borders=20 --no-buttons --auto-close --height=100 --title=$"Downloading..."
			echo "Finished downloading"
			
			### Create packagelist
			while true; do 
				echo $"# Updating the local package list";
				dpkg-scanpackages --multiversion ./ /dev/null | gzip -9c > ./Packages.gz 2>>0_errors.log
			break
			done | yad --progress --pulsate --center --borders=20 \
			--progress-text="Updating the local package list" \
			--no-buttons --auto-close --height=100 --title=$"Checking packages"
			
			### Check whether all packages were downloaded
			if [ ! -s $DOWNLOAD_LIST ]; then
				### Create .repo file
				echo "INSTALL_PACKAGES=${PACKAGE_LIST}" > "${D_DIRECTORY}/${PACKAGE_NAME%:*}_${SYSTEM_ARCH}.repo"
				echo "PACKAGE_DESCRIPTION=${PACKAGE_DESC}" >> "${D_DIRECTORY}/${PACKAGE_NAME%:*}_${SYSTEM_ARCH}.repo"
				cat $TEMP_FILE >> "${D_DIRECTORY}/${PACKAGE_NAME%:*}_${SYSTEM_ARCH}.repo"
				### Inform the user of the positive result
				yad --center --width=350 --text-align=center \
				--text=$"<b>$PACKAGE_NAME</b> and its dependencies were downloaded.\n\
\n Your local Repo packagelist has been updated.\n\
A new file <b>${PACKAGE_NAME%:*}_${SYSTEM_ARCH}.repo</b> was created." --margins=5 \
				--button='gtk-close':1 --title=$"Packagelist created" --borders=5 \
				--window-icon=gtk-dialog-info --image="gtk-dialog-info"
			### If there are missing packages or they didn't download properly
			else
				### Save errors in log
				export DOWNLOAD_LIST=$DOWNLOAD_LIST
				while read line; do
					echo "	Error: $line did not download properly (or was skipped)." >>0_errors.log
				done <"$DOWNLOAD_LIST"
				### Inform the user of the negative result
				yad --center --width=480 --text-align=center \
				--text=$"<b>$PACKAGE_NAME</b>\n and/or some of its dependencies could not be saved.\n\n\
Possible reasons: The download couldn't start, or was stopped abruptly.\n\
Click the <b>List</b> button to review the list of failed//skipped packages.\n\n\
Your local Repo packagelist has been updated." \
				--button=$"LIST":'bash -c "list_packages \"$DOWNLOAD_LIST\""' --margins=5 \
				--button='gtk-close':1 --title=$"Error Downloading" --borders=5 \
				--window-icon=gtk-dialog-error --image="gtk-dialog-error"
			fi
		else
			### Clean packages and sourcelist
			if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
				clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
			fi
			### Clean temp file
			remove_temp
		fi
		### Clean packages and sourcelist
		if [[ "$PACKAGE_SOURCE" == "PI" ]]; then
			clean_pi_repo "$PRE_INSTRUCTIONS" "$INSTRUCTIONS" "$POST_INSTRUCTIONS" "$NEW_SOURCE"
		fi
		### Clean temp file
		remove_temp
	fi
}

#### Search for packages and list all matches ####
search_package(){
	local PACKAGE_NAME="${1}"
	local REPO_DIRECTORY="${2}"
	
	### Create temp file to contain package search entries
	SEARCH_LIST=$(mktemp)
	
	### Load Search options:
	APT_MATCH="$(check_config "LOAD" "APT_MATCH")"
	SEARCH_MATCH="$(check_config "LOAD" "SEARCH_MATCH")"
	local PRE_NAME=""
	local POST_NAME=""
	### Setting Special search filters
	if [[ "$SEARCH_MATCH" == "strict" ]] || [[ "$SEARCH_MATCH" == "exact" ]]; then
		PRE_NAME="^"
	fi
	if [[ "$SEARCH_MATCH" == "exact" ]]; then
		POST_NAME='$'
	fi
	
	### Searching... dialog
	while true; do
		### Check for packagename in /usr/share/packageinstaller-pkglist
		if [[ "$REPO_DIRECTORY" == "" ]] && [ -d /usr/share/packageinstaller-pkglist ]; then
			echo $"# Searching antiX package installer (PI)"
			
			### move to antiX package installer folder to process each file
			cd /usr/share/packageinstaller-pkglist
			
			### create local variables
			local PI_NAME
			local PI_DESCRIP
			local SKIP_FILE
			local GREP_STRING="cat $PI_CONTENT"
			### skip other architecture
			if [[ "$SYSTEM_ARCH" == "amd64" ]]; then SKIP_FILE="32"; fi
			if [[ "$SYSTEM_ARCH" == "i386" ]]; then SKIP_FILE="64"; fi
			
			## Check if PI_CONTENT doesn't exist, and generate PI_CONTENT file from .pm fies
			if [ ! -s "$PI_CONTENT" ]; then
				### Add all package names and descriptions to PI_CONTENT
				for file in *.pm; do
					### Package "name"
					PI_NAME="${file%.*}"
					
					### skip all VPN packages and all non architecture packages
					if [ $(echo "$PI_NAME" | grep -ic "vpn\|deb_package_tools") -eq 0 ] && [[ "$(awk '/<installable>/{getline; print}' "$file")" != "${SKIP_FILE}" ]]; then
						### Package description
						PI_DESCRIP="$(awk '/<description>/{getline; print}' "$file")"
						### Add info in new line to PI_CONTENT file
						echo "${PI_NAME}|${PI_DESCRIP}" >> "$PI_CONTENT"
					fi
				done
			fi
			
			### Creating grep command string
			for word in $PACKAGE_NAME; do
				GREP_STRING="$GREP_STRING | grep -i ${PRE_NAME}${word}${POST_NAME}"
			done
			
			### search for matches in PI_CONTENT
			while read -r line; do
				if [[ "$line" != "" ]]; then
					PI_NAME="$(echo "$line" | cut -d "|" -f 1)"
					PI_DESCRIP="$(echo "$line" | cut -d "|" -f 2-)"
					### save package info to file
					echo "PI" >> "$SEARCH_LIST"
					echo "$PI_NAME" >> "$SEARCH_LIST"
					echo "$PI_DESCRIP" >> "$SEARCH_LIST"
				fi
			done < <(eval $GREP_STRING)
		### Check for .repo files and list results
		elif [[ "$REPO_DIRECTORY" != "" ]] && [ -d "$REPO_DIRECTORY" ]; then
			echo $"# Searching .repo files"
			
			### Check if correct folder
			REPO_DIRECTORY="$(check_repo_folder "$REPO_DIRECTORY")"
			
			### Move to the repo folder
			cd "$REPO_DIRECTORY"
			
			### create local variables
			local REPO_NAME
			local REPO_DESC
			### Create temp file for processing info
			TEMP_REPO=$(mktemp)
			
			### Search for .repo files and store their information
			for file in *.repo; do
				if [[ "$file" == *"${SYSTEM_ARCH}.repo" ]]; then
					REPO_NAME="${file%_*}"
					REPO_DESC="$(cat "$file" | grep -m1 "PACKAGE_DESCRIPTION=" | cut -d "=" -f 2-)"
					if [[ "$REPO_DESC" == "" ]]; then REPO_DESC=" "; fi
					echo "${REPO_NAME}|${REPO_DESC}" >> $TEMP_REPO
				fi
			done
			
			# GREP file for searching
			local GREP_STRING="cat $TEMP_REPO"
			### Creating grep command string
			for word in $PACKAGE_NAME; do
				GREP_STRING="$GREP_STRING | grep -i ${PRE_NAME}${word}${POST_NAME}"
			done
			
			### search for matches in REPO files
			while read -r line; do
				if [[ "$line" != "" ]]; then
					REPO_NAME="$(echo "$line" | cut -d "|" -f 1)"
					REPO_DESC="$(echo "$line" | cut -d "|" -f 2-)"
					### save package info to file
					echo "REPO" >> "$SEARCH_LIST"
					echo "$REPO_NAME" >> "$SEARCH_LIST"
					echo "$REPO_DESC" >> "$SEARCH_LIST"
				fi
			done < <(eval $GREP_STRING)
			
			### Remove TEMP_REPO file
			rm -f -- "$TEMP_REPO"
		fi
		
		## More control on results (filtering)
		### restarting Search filter values
		PRE_NAME=""
		POST_NAME=""
		if [[ "$APT_MATCH" == "strict" ]] || [[ "$APT_MATCH" == "exact" ]]; then
			PRE_NAME="^"
		fi
		if [[ "$APT_MATCH" == "exact" ]]; then
			POST_NAME='$'
		fi
		
		### Check for packages with apt-search
		echo $"# Searching APT list";
		while read -r line; do
			echo "APT" >> "$SEARCH_LIST"
			echo "$line" | cut -d " " -f 1 >> "$SEARCH_LIST"
			echo "$line" | cut -d " " -f 3- >> "$SEARCH_LIST"
		done < <(apt-cache search ${PRE_NAME}${PACKAGE_NAME}${POST_NAME})
		### get out of this loop
		break
	done | yad --progress --pulsate --center --borders=20 \
		--progress-text=$"Searching for packages"  \
		--no-buttons --auto-close --height=100 --title=$"Searching"
	
	## Display all found results for search if list is NOT empty
	if [[ -s $SEARCH_LIST ]]; then
		PACKAGE_SELECTED=$(yad --class="apt-offline-repo" --name="List-Packages" \
--window-icon=synaptic --title=$"Select Package" --borders=20 \
--text=$"Search parameters: <b>$PACKAGE_NAME</b>\nSelect a package from the list" \
--text-align=center --center --separator="|" --list  --search-column=3 --column=" " \
--column=$"Package" --column=$"Description" --width=700 --height=400 \
--button=gtk-ok:0 --button=gtk-quit:1 < $SEARCH_LIST)
		if [ -z "$PACKAGE_SELECTED" ]; then PACKAGE_SELECTED="+nothing+"; fi
		echo "${PACKAGE_SELECTED%|*}"
	else
		### Removing temp file
		rm -f -- "$SEARCH_LIST"
		echo ""
		return 1
	fi
	
	### Removing temp file
	rm -f -- "$SEARCH_LIST"
}

#### UPDATE PACKAGE LIST ####
update_apt(){
	local U_OPTION="${1}"
	local SOURCES_FILE=/etc/apt/sources.list.d/local.list
	
	### Update ONLY local repo lists
	if [[ "$U_OPTION" == "Repo" ]] && [ -f "$SOURCES_FILE" ]; then
		echo "Updating package list"
		x-terminal-emulator -T $"apt update" -e /bin/bash -c \
			"sudo apt-get update -o Dir::Etc::sourcelist=\"sources.list.d/local.list\" \
			-o Dir::Etc::sourceparts=\"-\" -o APT::Get::List-Cleanup=\"1\" && sleep 0.1 &&
			yad --center --width=250 --text-align=center --text=\"$YAD_FINISHED\" \
			--button='gtk-ok':1 --title='apt - Update' "
	### Normal update of lists
	else
		### Open terminal and ask if the user wants to install
		check_sudo "true"
		if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
			### update packagelist
			echo "updating package list"
			while true; do
				echo $"# Updating package list"
				check_sudo apt update
				yad --center --width=250 --text-align=center --text="$YAD_FINISHED" \
					--button='gtk-ok':1 --title='apt - Update'
				break
			done | yad --progress --pulsate --center --borders=20 \
				--progress-text=$"Updating packagelist"  \
				--no-buttons --auto-close --height=100 --title="apt - Update"
		fi
	fi
}

#### INSTALL SELECTED PACKAGE ####
install_package(){
	local PACKAGE_NAME="${1}"
	## Check if DPKG is locked
	if check_sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; then
		error_dialog "DPKG" "LOCKED"
		return 1
	## Check if PACKAGE_NAME is NOT in packagelist
	elif [[ "$PACKAGE_NAME" == "" ]] || [ "$(apt-cache show $PACKAGE_NAME | grep -Ec ${PACKAGE_NAME%:*} 2>/dev/null)" -eq 0 ]; then
		error_dialog "NoPackage" "$PACKAGE_NAME"
	## Ask the user if they want to install PACKAGE_NAME
	else
		x-terminal-emulator -T $"install $PACKAGE_NAME" -e /bin/bash -c \
		"sudo apt install $PACKAGE_NAME && sleep 0.1 &&
		yad --center --width=250 --text-align=center --text=\"$YAD_FINISHED\" \
		--button='gtk-ok':1 --title='APT - Install'"
	fi
}

########################################################################
#######################  3. CHECKING FUNCTIONS  ########################
########################################################################

#### Check if user runs as sudo ####
check_sudo(){
	local EXE_STRING="${@}"
	local SUDO_COMMAND="sudo"
	
	#If sudo is active or the user is root
	if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
		SUDO_COMMAND="sudo"
	## Check if not root
	elif [[ $EUID -ne 0 ]]; then
		# gksu exists
		if [ -x /usr/bin/gksu ]; then SUDO_COMMAND="gksudo";
		### su-to-root exists
		elif [ -x /usr/bin/su-to-root ]; then SUDO_COMMAND="su-to-root -X -c";
		# simply use sudo
		else SUDO_COMMAND="x-terminal-emulator -T \"Give SU powers\" -e /bin/bash -c \"sudo\"";
		fi
	fi
	#run sudo command
	eval "$SUDO_COMMAND $EXE_STRING"
	
	#~ if [[ $EUID -eq 0 ]] || sudo true; then
		#~ ## Return success
		#~ return 0
	#~ else
		#~ ## Inform user of error ###
		#~ echo "not root or sudo password error" >> 0_errors.log
		#~ error_dialog "Password" "Password"
		#~ ## Return failure
		#~ return 1
	#~ fi
}

#### CHECK IF DEPENDENCIES ARE INSTALLED ####
check_depends(){
	### Check if dpkg-scanpackages is installed
	if [ ! -x /usr/bin/dpkg-scanpackages ]; then
		### Ask if app should be installed
		yad --image="gtk-dialog-info" --borders=10 --center \
		--title=$"Missing dependencies" --class="apt-offline-repo" --name="install-check" \
		--form --align=center --separator="" \
		--field=$"the 'dpkg-dev' utilities are not currently installed on your system":LBL '' \
		--field=$"Do you want to install them? (2MB)":LBL '' \
		--button=gtk-yes:0 --button=gtk-quit:1
		local exitcode=$?
		if [ $exitcode -eq 0 ]; then
			### install dependencies
			install_package "dpkg-dev"
		fi
		return 1
	else
		### launch Download Dependencies window
		return 0
	fi
}

#### Check and fix folder depending on architecture ####
check_repo_folder(){
	local D_DIRECTORY="${1}"
	local SELECTED_ARCH="${2}"
	
	if [[ "$D_DIRECTORY" == "*/" ]]; then D_DIRECTORY="${D_DIRECTORY%/*}"; fi
	### Check if the current directory D_DIRECTORY is inside a folder in the repo directory
	if [[ "$D_DIRECTORY" == *"/amd64" ]] || [[ "$D_DIRECTORY" == *"/i386" ]]; then
		D_DIRECTORY="${D_DIRECTORY%/*}"
	fi
	### Check architecture-specific subdirectory exists (create it, if necessary)
	if [[ "$SELECTED_ARCH" != "" ]] && [ ! -d "${D_DIRECTORY}/${SELECTED_ARCH}" ]; then
		mkdir "${D_DIRECTORY}/${SELECTED_ARCH}" 2>/dev/null
	fi
	
	echo "$D_DIRECTORY"
}

#### Check, fix or save configuration options ####
check_config(){
	CONFIG_TASK="${1}"
	CONFIG_VAR="${2}"
	CONF_VALUE="${3}"
	local CONF_FILE="${SCRIPT_FOLDER}/my-offline-repo.conf"
	
	### Subfunction to check variable validity
	var_check(){
		local CONFIG_VAR="${1}"
		local CONF_VALUE="${2}"
		case $CONFIG_VAR in
			DOWNLOAD_DIR)
				### DOWNLOAD_DIR must be a valid directory. Defaults to "$XDG_DOWNLOAD_DIR"
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^DOWNLOAD_DIR=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [[ "$CONF_VALUE" == "" ]] || [ ! -d "$CONF_VALUE" ]; then
					CONF_VALUE="$XDG_DOWNLOAD_DIR"
				fi
				;;
			INSTALL_DIR)
				### INSTALL_DIR must be a valid directory. Defaults to "$XDG_DOWNLOAD_DIR"
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^INSTALL_DIR=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [[ "$CONF_VALUE" == "" ]] || [ ! -d "$CONF_VALUE" ]; then
					CONF_VALUE="$XDG_DOWNLOAD_DIR"
				fi
				;;
			DEPENDENCY_MODE)
				### DEPENDENCY_MODE must be: minimum, 2-levels, compromise, or ALL. (Defaults to ALL)
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^DEPENDENCY_MODE=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "minimum\|2-levels\|compromise\|ALL") -eq 0 ]; then
					CONF_VALUE="ALL"
				fi
				;;
			EXCLUDE_LIST)
				### EXCLUDE_LIST can be true or false. Defaults to false
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^EXCLUDE_LIST=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "true\|false") -eq 0 ]; then
					CONF_VALUE="false"
				fi
				;;
			APT_MATCH)
				### APT_MATCH must be normal, sctrict or exact. Defaults to strict
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^APT_MATCH=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "normal\|strict\|exact") -eq 0 ]; then
					CONF_VALUE="strict"
				fi
				;;
			SEARCH_MATCH)
				### SEARCH_MATCH must be normal, sctrict or exact. Defaults to normal
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^SEARCH_MATCH=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "normal\|strict\|exact") -eq 0 ]; then
					CONF_VALUE="normal"
				fi
				;;
			REMOVE_OLD)
				### REMOVE_OLD can be true or false. Defaults to false
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^REMOVE_OLD=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "true\|false") -eq 0 ]; then
					CONF_VALUE="false"
				fi
				;;
			AUTO_CLEAN)
				### AUTO_CLEAN can be true or false. Defaults to true
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^AUTO_CLEAN=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "true\|false") -eq 0 ]; then
					CONF_VALUE="true"
				fi
				;;
			UPDATE_EXTRA)
				### UPDATE_EXTRA can be true or false. Defaults to false
				if [[ "$CONF_VALUE" == "" ]]; then
					CONF_VALUE="$(cat $MOR_CONF | grep "^UPDATE_EXTRA=" | cut -d "=" -f 2-)"
				fi
				### Remove spaces
				CONF_VALUE="$(echo $CONF_VALUE | sed 's/ //g')"
				### Set default if not valid
				if [ $(echo "$CONF_VALUE" | grep -xci "true\|false") -eq 0 ]; then
					CONF_VALUE="false"
				fi
				;;
			*)
				error_dialog "OPTION" "$CONFIG_VAR"
				CONFIG_VAR="Error: Not a valid property"
				return 1 ;;
		esac
		### echo the asked value after being checked
		echo "$CONF_VALUE"
	}
		
	### Import configuration file to temp
	if [[ "$CONFIG_TASK" == "IMPORT" ]]; then
		### Check if config file exists
		if [ -f $CONF_FILE ]; then
			### Import config file
			cat "$CONF_FILE" > $MOR_CONF
			echo "Configuration file found. Importing options."
		fi
		
	### Load specific configuration option
	elif [[ "$CONFIG_TASK" == "LOAD" ]] && [[ "$CONFIG_VAR" != "" ]]; then
		CONF_VALUE="$(var_check "$CONFIG_VAR")"
		echo  "$CONF_VALUE"
		
	### Check and save configuration change
	elif [[ "$CONFIG_TASK" == "SAVE" ]] && [[ "$CONFIG_VAR" != "" ]]; then
		### Check if value is valid
		CONF_VALUE="$(var_check "$CONFIG_VAR" "$CONF_VALUE")"
		if [[ "CONF_VALUE" != "Error: Not a valid property" ]]; then
			### Save value to temp config file
			sed -i -e "/${CONFIG_VAR}=/d" "$MOR_CONF"
			echo "${CONFIG_VAR}=${CONF_VALUE}" >> $MOR_CONF
			### Save value to local config file
			sed -i -e "/${CONFIG_VAR}=/d" "$CONF_FILE"
			echo "${CONFIG_VAR}=${CONF_VALUE}" >> $CONF_FILE
		fi
		
	else
		echo "Error: Invalid Task"
	fi
}

########################################################################
#######################  4. OTHER MAIN FUNCTIONS  ######################
########################################################################

#### BUILD DEPENDENCY LIST FOR PACKAGE ####
build_dependencies(){
	local PACKAGE_NAME="${1}"
	local DEPENDENCY_LIST="${2}"
	local PACKAGE_SOURCE="${3}"
	echo "Package: $PACKAGE_NAME, saved to ${DEPENDENCY_LIST}, using source +${PACKAGE_SOURCE}+"
	### Load DEPENDENCY_MODE from options
	DEPENDENCY_MODE="$(check_config "LOAD" "DEPENDENCY_MODE")"
	echo "Dependency list based on: $DEPENDENCY_MODE"
	
	### Exception for packages libreoffice-help-, as they try to download too many browsers
	if [[ "$PACKAGE_NAME" == "libreoffice-help-"* ]]; then
		PACKAGE_NAME="$(apt-cache $PACKAGE_SOURCE depends "$PACKAGE_NAME" --no-recommends \
		--no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances | \
		grep -E ': ' | grep -v 'firefox\|konqueror\|chromium\|epiphany' | \
		cut -d ':' -f 2,3 | sed -e s/'<'/''/ -e s/'>'/''/ | sort -u)"
	fi
	
	### MINIMUM dependency option
	if [ $(echo "$DEPENDENCY_MODE" | grep -ic "minimum") -gt 0 ]; then
		apt-cache $PACKAGE_SOURCE depends $PACKAGE_NAME --no-recommends --no-suggests \
		--no-conflicts --no-breaks --no-replaces --no-enhances | sed 's/^.*: //g' | \
		sed 's/ //g' | sed -e s/'<'/''/ -e s/'>'/''/ | sort -u >> $DEPENDENCY_LIST
	### 2-levels dependency option
	elif [ $(echo "$DEPENDENCY_MODE" | grep -ic "2-levels") -gt 0 ]; then
		PACKAGE_NAME="$(apt-cache $PACKAGE_SOURCE depends $PACKAGE_NAME --no-recommends \
		--no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances | \
		sed 's/^.*: //g' | sed 's/ //g' | sed -e s/'<'/''/ -e s/'>'/''/ | sort -u)"
		apt-cache $PACKAGE_SOURCE depends $PACKAGE_NAME --no-recommends --no-suggests \
		--no-conflicts --no-breaks --no-replaces --no-enhances | sed 's/^.*: //g' | \
		sed 's/ //g' | sed -e s/'<'/''/ -e s/'>'/''/ | sort -u >> $DEPENDENCY_LIST
	### compromise dependency option
	elif [ $(echo "$DEPENDENCY_MODE" | grep -ic "compromise") -gt 0 ]; then
		apt-cache $PACKAGE_SOURCE depends $PACKAGE_NAME --recurse --no-recommends \
		--no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances \
		-o APT::Cache::ShowOnlyFirstOr=true | grep "^\w" | sort -u >> $DEPENDENCY_LIST
	### ALL dependency option
	elif [ $(echo "$DEPENDENCY_MODE" | grep -ic "ALL") -gt 0 ]; then
		apt-cache $PACKAGE_SOURCE depends $PACKAGE_NAME --recurse --no-recommends \
		--no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances | \
		grep "^\w" | sort -u >> $DEPENDENCY_LIST
	fi
}

#### SEARCH AND INSTALL PACKAGES ####
search_install(){
	### Retrieve user input
	local REPO_DIRECTORY="${1}"
	local PACKAGE_NAME="${2}"
	local SELECTED_ARCH="$SYSTEM_ARCH"
	local PACKAGE_SOURCE="APT"
	
	### Check if the selected directory is a repo folder
	REPO_DIRECTORY="$(check_repo_folder "$REPO_DIRECTORY")"
	
	### Search for package
	local PACKAGE_INFO
	PACKAGE_INFO="$(search_package "$PACKAGE_NAME" "$REPO_DIRECTORY")"
	if [[ "$PACKAGE_INFO" == "" ]]; then
		error_dialog "NoPackage" "$PACKAGE_NAME";
		return 1
	elif [[ "$PACKAGE_INFO" == "+nothing+" ]]; then
		error_dialog "NoSelection" "$PACKAGE_NAME";
		return 1
	fi
	
	### Replace info for selected package
	PACKAGE_SOURCE="$(echo "$PACKAGE_INFO" | cut -d "|" -f 1)"
	PACKAGE_NAME="$(echo "$PACKAGE_INFO" | cut -d "|" -f 2)"
	local PACKAGE_DESC="$(echo "$PACKAGE_INFO" | cut -d "|" -f 3-)"
	echo "Package selected: $PACKAGE_NAME from $PACKAGE_SOURCE"
	
	### If REPO package selected, process info and check if available
	if [[ "$PACKAGE_SOURCE" == "REPO" ]]; then
		#retrieve packages to install from .repo
		cd "$REPO_DIRECTORY"
		local INSTALL_PACKAGES="$(cat "${PACKAGE_NAME}_${SYSTEM_ARCH}.repo" | grep "INSTALL_PACKAGES" | cut -d "=" -f 2-)"
	else
		INSTALL_PACKAGES="$PACKAGE_NAME"
	fi
	
	#### INSTALL PACKAGES IF SUDO/ROOT ###
	check_sudo "true"
	if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
		echo "Installing $INSTALL_PACKAGES"
		install_package "$INSTALL_PACKAGES"
	fi
}

#### ENABLE OFFLINE REPO IN APT.LIST ####
repo_update(){
	local D_DIRECTORY="${1}"
	local R_ACTION="${2}"
	local SOURCES_FILE=/etc/apt/sources.list.d/local.list
	local S_COMMENT="# My Offline Repo"
	local S_HEAD="deb [ trusted=yes ] file"
	local S_REPO="${S_HEAD}:${D_DIRECTORY} ./"
	echo "$D_DIRECTORY and $R_ACTION"
	
	## Create Source file if not already there
	if [ ! -f "$SOURCES_FILE" ]; then
		check_sudo "true"
		if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
			[ -d "/etc/apt/sources.list.d" ] && check_sudo touch "$SOURCES_FILE"
			echo "$SOURCES_FILE created"
		fi
	fi
	
	### Remove any local repo from sourcelist ##
	remove_repo(){
		### Check if Local Repo is inside the sourcelist
		if [ "$(cat $SOURCES_FILE | grep -xc "$S_COMMENT")" -ne 0 ]; then
			check_sudo "true"
			if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
				#delete offline repo entry
				check_sudo "sed -i -e \"/${S_COMMENT}/d\" \"$SOURCES_FILE\""
				check_sudo sed -i -e "/file:/d" "$SOURCES_FILE"
				echo "Removed local repo from $SOURCES_FILE"
			fi
		fi
	}
		
	### Ensure the sources file exists
	if [ ! -f "$SOURCES_FILE" ]; then
		error_dialog "Sourcefile" "$SOURCES_FILE"
	### Remove all repos from sourcefiles
	elif [[ "$R_ACTION" == "1" ]]; then
		### remove repo from sourcelist
		remove_repo
		### Update system packagelist
		update_apt
	### Check if the Repo path is broken
	elif [ ! -d "$D_DIRECTORY" ] && [[ "$R_ACTION" == "0" ]]; then
		error_dialog "Directory" "$D_DIRECTORY"
	### Add repo to sourcefiles
	elif [[ "$R_ACTION" == "0" ]]; then
		#Navigate to folder
		cd "$D_DIRECTORY"
		#### Check if Packages.gz file is missing
		if [ ! -f Packages.gz ]; then
			## Check if the repo folder has a structure used by this program
			D_DIRECTORY="$(check_repo_folder "$D_DIRECTORY")"
			if [ -d "${D_DIRECTORY}/${SYSTEM_ARCH}" ]; then
				D_DIRECTORY="${D_DIRECTORY}/${SYSTEM_ARCH}"
			fi
			echo "$D_DIRECTORY"
			cd "$D_DIRECTORY"
			
			## Rebuild S_REPO string
			S_REPO="${S_HEAD}:${D_DIRECTORY} ./"
			### check for a second time for Packages.gz file
			if [ ! -f Packages.gz ]; then
				error_dialog "Repopath" "$D_DIRECTORY"
				return 1
			fi
		fi
		## Check if root ###
		check_sudo "true"
		if [[ $EUID -eq 0 ]] || $(sudo -n true 2>/dev/null); then
			### Remove any previous local repo
			remove_repo
			### Add new repo path to sourcefile
			echo "$S_COMMENT" | check_sudo tee -a "$SOURCES_FILE" > /dev/null
			echo "$S_REPO" | check_sudo tee -a "$SOURCES_FILE" > /dev/null
			echo "Added $D_DIRECTORY to sourcefiles"
			### Update system packagelist
			update_apt "Repo"
		fi
	fi
}

cleanup() {
    ### Remove temporary files
    rm -f -- "$PI_CONTENT"
    rm -f -- "$TEMP_FILE"
    rm -f -- "$TEMP_FILE2"
	rm -f -- "$PRE_INSTRUCTIONS"
	rm -f -- "$INSTRUCTIONS"
	rm -f -- "$POST_INSTRUCTIONS"
	rm -f -- "$MOR_CONF"
	rm -f -- "$EXIT_SCRIPT"
	echo "Cleanup and exiting"
}

### Set trap on EXIT for cleanup
trap cleanup EXIT

### Export dialog functions
export -f main_dialog
export -f download_dialog
export -f update_dialog
export -f repo_dialog
export -f help_dialog
export -f error_dialog
export -f conf_dialog

### Export functions
export -f download_updates
export -f download_dependencies
export -f search_package
export -f build_dependencies
export -f check_sudo
export -f check_depends
export -f check_repo_folder
export -f check_config
export -f update_apt
export -f search_install
export -f install_package
export -f repo_update

### Export variables
export SYSTEM_ARCH=$SYSTEM_ARCH
export SCRIPT_FOLDER=$SCRIPT_FOLDER
export PI_CONTENT=$PI_CONTENT
export MOR_CONF=$MOR_CONF
export EXIT_SCRIPT=$EXIT_SCRIPT
export YAD_FINISHED="$YAD_FINISHED"
export THIS_MACHINE

#### IMPORT CONFIG ####
check_config "IMPORT"

#### START PROGRAM ####
main_dialog

while [ $(cat $EXIT_SCRIPT | grep -c "false") -eq 1 ]; do
	sleep 1
done
